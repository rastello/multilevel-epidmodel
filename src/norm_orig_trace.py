
import math
import getopt

import numpy as np
import matplotlib.pyplot as plt

from yaml import load as yamlload, dump as yamldump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from parse_seepia import write_trace


# Extract the infected from the trace
def get_list_infected(trace):
    ltrace = trace["trace"]
    
    linfected = []
    for i in range(0, len(ltrace)):
        linfected = linfected + [ltrace[i]["all"]["infected"]]

    return linfected

#def get_trace_from_infected(trace_orig, nlinfected):



    # TODO


#    return trace_norm


#def print_out_trace(trace, filename):

    # TODO: recover phase, window and regionf from "trace"


#    traceall = True


    # Calling Parse_seepia.write_trace
#    write_trace(filename, phase, window, regionf, traceall)




# Visualize the infected (plot it)
def plot_infected(lldata, lnames=[]):
    nplot = len(lldata)
    assert(nplot>0)
    if (lnames!=[]):
        assert(nplot==len(lnames))

    lenplot = len(lldata[0])
    for i in range(1, nplot):
        assert(len(lldata[i])==lenplot)
    
    T=[*range(0,lenplot)]
    for i in range(0, nplot):
        plt.plot(T, lldata[i])
    if (lnames!=[]):
        plt.legend(lnames)
    plt.xlabel('Time (days)')
    plt.show()

# =====================================================

def get_derivative(ldata):
    lder = [(ldata[1] - ldata[0]) / 2]
    for i in range (1, len(ldata)):
        temp = (ldata[i] - ldata[i-1]) / 2
        lder = lder + [temp]
    return lder

# Compute the average of ldata[min] up to (and including) ldata[max]
def get_average(ldata, min, max):
    temp = 0.0
    for i in range(min, max+1):
        temp += ldata[i]
    return temp / (max-min+1)

# Get the list of average over an area
# Returned list has the same length than ldata
def get_data_average(ldata, local_radius):
    N = len(ldata)
    laverage = []
    for i in range(0, local_radius):
        laverage = laverage + [get_average(ldata, 0, 2*i)]
    for i in range(local_radius, N-local_radius):
        laverage = laverage + [get_average(ldata, i-local_radius, i+local_radius)]
    for i in range(N-local_radius, N):
        laverage = laverage + [get_average(ldata, i-(N-1-i), N-1)]
    return laverage

def detect_peak_hole(lloginfected, laverage, coeff_tolerance):
    N = len(lloginfected)
    ltolerancemin = []
    ltolerancemax = []
    lpeak = []
    lhole = []
    for i in range(0, N):
        
        #tolerance = coeff_tolerance * lloginfected[i]
        
        coeff_tolerance = 1.96 / math.sqrt(math.exp(laverage[i]))
        tolerance = coeff_tolerance*laverage[i]
        
        ltolerancemin = ltolerancemin + [laverage[i]-tolerance]
        ltolerancemax = ltolerancemax + [laverage[i]+tolerance]

        delta = (lloginfected[i] - laverage[i])
        if (delta>tolerance):
            lpeak += [i]
        if (delta<-tolerance):
            lhole += [i]
    return (ltolerancemin, ltolerancemax, lpeak, lhole)

# Check the interval [ipeak-max_lateness, ipeak[ for holes
def get_holes_before(ipeak, max_lateness, lholes):
    lret = []
    for ihole in lholes:
        if ( ((ipeak-max_lateness)<=ihole) and (ihole<ipeak)):
            lret = lret + [ihole]
    return lret

def fill_hole_from_peak(lloginfected, laverage, ltolerancemin, lpeak, lhole, max_lateness):
    N = len(lloginfected)
    lloginfected1 = lloginfected[0:N]
    for ipeak in lpeak:
        # Quota of patient to transfer in the previous holes
        quotadistr = lloginfected1[ipeak] - laverage[ipeak]

        # Check the interval [ipeak-max_retard, ipeak] for holes
        lholes_right_before = get_holes_before(ipeak, max_lateness, lhole)

        # Fill them by priority from the latter to the earlier
        # => Fill everyone up to min_tolerance from earlier to latter?
        for ihole in lholes_right_before[::-1]:
            assert(quotadistr>=0)
            
            tofill = ltolerancemin[ihole] - lloginfected1[ihole]
            fill = min(tofill, quotadistr)

            lloginfected1[ihole] = lloginfected1[ihole] + fill
            quotadistr = quotadistr - fill
        lloginfected1[ipeak] = laverage[ipeak] + quotadistr
    return lloginfected1


# Main normalization function
def normalize_data(linfected):
    # Get the log of the data (due to the exponential nature of the law)
    N = len(linfected)
    lloginfected = []
    for i in range(0, N):
        if (linfected[i]==0)
            temp = -1
        else:
            temp = math.log(linfected[i])
        lloginfected = lloginfected + [temp]

    # Get the derivative (~ R)
    lder_loginfected = get_derivative(lloginfected)

    # Average over a area
    local_radius = 5
    laverage = get_data_average(lloginfected, local_radius)
    lder_average = get_data_average(lder_loginfected, local_radius)

    # DEBUG
    #plot_infected([lloginfected, laverage], ["log infected", "average log infected"])
    #plot_infected([lder_loginfected, lder_average], ["der log infected", "average der log infected"])


    # Peak/Hole detection
    # Newton approx of degree 1 => Finally not needed.
    coeff_tolerance = 0.0366
    (ltolerancemin, ltolerancemax, lpeak, lhole) = detect_peak_hole(lloginfected, laverage, coeff_tolerance)
    
    # DEBUG
    print("Peak (num= " + str(len(lpeak)) + ") = " + str(lpeak))
    print("Hole (num= " + str(len(lhole)) + ") = " + str(lhole))
    plot_infected([lloginfected, laverage, ltolerancemin, ltolerancemax],
        ["log infected", "average log infected", "tolerance min", "tolerance max"])

    
    # Filling the holes with the peaks:
    # a) Identifying how much to transfer per peak
    # b) Filling the holes with the peak to go into tolerance range
    # c?) Putting the rest of a peak uniformly in the past (up to D days, D=4?)
    max_lateness = 4
    lloginfected1 = fill_hole_from_peak(lloginfected, laverage, ltolerancemin,
        lpeak, lhole, max_lateness)

    # DEBUG
    #plot_infected([lloginfected, lloginfected1, laverage],
    #        ["log infected", "log infected arranged", "average log infected"])


    # === SECOND PASS
    laverage1 = get_data_average(lloginfected1, local_radius)
    #plot_infected([lloginfected1, laverage1], ["log infected", "average log infected"])

    (ltolerancemin1, ltolerancemax1, lpeak1, lhole1) = detect_peak_hole(
        lloginfected1, laverage1, coeff_tolerance)
    
    # DEBUG
    #print("Peak1 (num= " + str(len(lpeak1)) + ") = " + str(lpeak1))
    #print("Hole1 (num= " + str(len(lhole1)) + ") = " + str(lhole1))
    #plot_infected([lloginfected1, laverage1, ltolerancemin1, ltolerancemax1],
    #    ["log infected", "average log infected", "tolerance min", "tolerance max"])
    
    # Normal filling: peak distribute uniformly across D previous days
    lloginfected2 = lloginfected1[0:N]
    for ipeak in lpeak1:
        budget = lloginfected[ipeak] - ltolerancemax1[ipeak]
        budget_distr = budget / float(max_lateness)
        for k in range(max(0, ipeak-max_lateness), ipeak):
            lloginfected2[k] = lloginfected2[k] + budget_distr
        lloginfected2[ipeak] = ltolerancemax1[ipeak]

    plot_infected([lloginfected1, lloginfected2, laverage1],["log infected1", "log infected2", "laverage1"])

    # Going back to infected
    linfected2 = []
    for i in range(0, N):
        linfected2 = linfected2 + [math.exp(lloginfected2[i])]

    return linfected2

# =====================================================


# =====================================================

# === Trace ("temp_tuner_orig.trace") obtained through the following command line ===
# python3 -m parse_seepia --event=urgences -s -90 -N 7 --trace=temp_tuner_orig.trace --window=-90 --all

trace_orig_filename = "temp_tuner_orig.trace"
trace_norm_filename = "temp_tuner_orig.norm.trace"

def main():
    # Get the original trace
    trace_orig_file = open(trace_orig_filename, 'r')
    trace_orig = yamlload(trace_orig_file, Loader=Loader)
    #print(trace_orig)

    linfected = get_list_infected(trace_orig)
    linfected_norm = normalize_data(linfected)

    # Produce a new file for the trace
    #trace_norm = get_trace_from_infected(trace_orig, linfected_norm)

    # Output the trace in a new file (yaml)
    #print_out_trace(trace_norm, trace_norm_filename)


# Launch!
main()

