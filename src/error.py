#!/usr/bin/env python3

import sys
import getopt
import math
from sklearn.metrics import r2_score

from yaml import load as yamlload, dump as yamldump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

_debug=0
r2score=False

def evaluate(trace_orig, trace_simu, print_res, values):
    assert(len(trace_orig)==len(trace_simu))

    #print(trace_orig)
    #print(trace_simu)

    groupf = values['groupf']
    aggregate = values['aggregate']
    lwhat = values['what']

    # Default option
    if (lwhat==[]):
        lwhat = ['infected']

    groups=set()
    dates=[]

    # TODO: immuned, derivated from infected (accumulation)
    # immuned_orig[G].append(immuned_orig[G][-1]+infected_orig[G][-1])
    
    # Main keys (obtained from the traces)
    lmain_key = []
    lratio_key = []
    for what in lwhat:
        if what.startswith('ratio_'):

            lratio_key = lratio_key + [what[len('ratio_'):]]
        else:
            lmain_key = lmain_key + [what]

    #lmain_key = ['infected', 'immuned']
    #lratio_key = ['infected']       # Ratio keys (ratio_XXX , computed from XXX)
    #print(lmain_key)
    #print(lratio_key)

    lkeys = lmain_key[:]
    for rk in lratio_key:
        lkeys += ['ratio_' + rk]

    orig_stats = {}
    orig_stats_total = {}
    for k in lkeys:
        orig_stats[k] = {}
        orig_stats_total[k] = []

    simu_stats = {}
    simu_stats_total = {}
    for k in lkeys:
        simu_stats[k] = {}
        simu_stats_total[k] = []

    size={}
    size_total=0
    error={}
    error_total=0
    BIG=10

    for t in range(0,len(trace_orig)):
        dates.append(t)

        for k in lmain_key:
            orig_stats_total[k].append(0)
            simu_stats_total[k].append(0)

        for G in trace_orig[t]:
            assert(G in trace_simu[t])
            if groupf is not None:
                if G not in groupf: continue
            if G not in groups:
                assert(t==0)
                groups.add(G)
                size[G]=trace_orig[0][G]['size']
                assert(size[G]==trace_simu[0][G]['size'])
                size_total+=size[G]

                for k in lmain_key:
                    orig_stats[k][G] = [trace_orig[0][G][k]]
                    simu_stats[k][G] = [trace_simu[0][G][k]]
                for rk in lratio_key:
                    orig_stats['ratio_' + rk][G] = [0]
                    simu_stats['ratio_' + rk][G] = [0]

                error[G]=0
            else:
                for k in lmain_key:
                    orig_stats[k][G].append(trace_orig[t][G][k])
                    simu_stats[k][G].append(trace_simu[t][G][k])

                for rk in lratio_key:
                    if not orig_stats[rk][G][-2]==0:
                        orig_stats['ratio_' + rk][G].append(orig_stats[rk][G][-1]/orig_stats[rk][G][-2])
                    else:
                        orig_stats['ratio_' + rk][G].append(BIG)
                    if not simu_stats[rk][G][-2]==0:
                        simu_stats['ratio_' + rk][G].append(simu_stats[rk][G][-1]/simu_stats[rk][G][-2])
                    else:
                        simu_stats['ratio_' + rk][G].append(BIG)

            for k in lmain_key:
                orig_stats_total[k][-1] += orig_stats[k][G][-1]
                simu_stats_total[k][-1] += simu_stats[k][G][-1]

            # if what in {'infected', None}:
            #     error[G]+=(infected_orig[G][-1]-infected_simu[G][-1])**2/max(1,infected_orig[G][-1])
            # elif what=='log-infected':
            #     error[G]+=(math.log(infected_orig[G][-1])-math.log(infected_simu[G][-1]))**2
            # elif what=='immuned':
            #     error[G]+=(immuned_orig[G][-1]-immuned_simu[G][-1])**2
            # elif what=='log-immuned':
            #     error[G]+=(math.log(immuned_orig[G][-1])-math.log(immuned_orig[G][-1]))**2
            # elif what=='ratio-infected':
            #     if (ratio_infected_orig[G][-1] is not None) and (ratio_infected_simu[G][-1] is not None):
            #         error[G]+=(ratio_infected_orig[G][-1]-ratio_infected_simu[G][-1])**2
            #         #TODO: what to do when one is None and not the other?...
        
        for rk in lratio_key:
            if t>1 and not orig_stats_total[rk][-2]==0:
                orig_stats_total['ratio_'+rk].append(orig_stats_total[rk][-1]/orig_stats_total[rk][-2])
            else:
                orig_stats_total['ratio_'+rk].append(BIG)
            if t>1 and not simu_stats_total[rk][-2]==0:
                simu_stats_total['ratio_'+rk].append(simu_stats_total[rk][-1]/simu_stats_total[rk][-2])
            else:
                simu_stats_total['ratio_'+rk].append(BIG)
        # if aggregate is True:
        #     if what in {'infected', None}:
        #         error_total+=(infected_total_orig[-1]-infected_total_simu[-1])**2/max(1,infected_total_orig[-1])**2
        #     
        #     elif what=='immuned':
        #         error_total+=(immuned_total_orig[-1]-immuned_total_simu[-1])**2
        #     
        #     elif what=='ratio-infected':
        #         if (ratio_infected_total_orig[-1] is not None) and (ratio_infected_total_simu[-1] is not None):
        #             error_total+=(ratio_infected_total_orig[-1]-ratio_infected_total_simu[-1])**2
        #     else: assert(False)

    # if aggregate is False:
    #     for G in trace_orig[0]:
    #         error_total+=error[G]
    #     error_total/=len(trace_orig[0])

    lres = []
    for what in lwhat:
        assert (what not in {'log-infected', 'log-immuned'}), what+' not handled yet'
        assert (what in orig_stats.keys()), what+' unknown'

        if r2score:
            if aggregate is True:
                res = r2_score(orig_stats_total[what], simu_stats_total[what])
            else:
                orig, simu = [], []
                for G in groups:
                    orig.append(orig_stats[what][G])
                    simu.append(simu_stats[what][G])
                res=r2_score(orig, simu, multioutput='variance_weighted')

            res = abs(1-res)
        else:
            if aggregate is True:
                res = sum([(o-s)**2 for (o,s) in zip(orig_stats_total[what],simu_stats_total[what])])
            else:
                res = 0
                for G in groups:
                    res += sum([(o-s)**2 for (o,s) in zip(orig_stats[what][G],simu_stats[what][G])])

        lres = lres + [res]

    # Merge all res
    final_res = 0.0
    for r in lres:
        final_res += r

    if print_res:
        print(final_res)
    return final_res
    



def read_traces_and_evaluate(values):
    filest = values['filest']
    
    assert(len(filest)==2)
    filename_orig = filest[0]
    filename_simu = filest[1]

    stream_orig=open(filename_orig, 'r')
    memt_orig=yamlload(stream_orig, Loader=Loader)
    trace_orig=memt_orig['trace']
    stream_simu=open(filename_simu, 'r')
    memt_simu=yamlload(stream_simu, Loader=Loader)
    trace_simu=memt_simu['trace']

    return evaluate(trace_orig, trace_simu, True, values)


# =====================================================================
# =====================================================================
# =====================================================================

# Default values
default_values = {}
default_values['groupf'] = None
default_values['aggregate'] = False
default_values['what'] = []
default_values['filest'] = []

def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[-t|--traces= file_orig,file_simu]'+
          '\n\t[-a|--aggregate]'+
          '\n\t[-s|--stat= infected|immuned|log_infected|log_immuned|ratio_infected]'+
          '\n\t[-g|--groups= group1,group2,...] restricts to those groups'
          )



def parse_opts_error_script(values, opts):
    for opt, arg in opts:                
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        elif opt in ("-t", "--traces"):
            values['filest']=arg.split(',')
        elif opt in ("-g", "--groups"): 
            values['groupf']=arg.split(',')
        elif opt in ("-a", "--aggregate"):
            values['aggregate']=True
        elif opt in ("-s", "--stat"):
            larg = arg.split(',')
            #for arg in larg:
            #    assert(arg in {'infected', 'immuned', 'log-infected', 'log-immuned', 'ratio_infected'})
            values['what']=larg
    return values


def get_values_error_script(opts):
    values = parse_opts_error_script(default_values, opts)
    return values

def main(argv):
    values = {}
    for (k,v) in default_values.items():
        values[k] = v

    try:                                
        opts, args = getopt.getopt(argv, "ht:as:g:", ["help", "traces=", "aggregate", "stat=", "groups=", "aggregate"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    
    values = parse_opts_error_script(values, opts)
    
    assert(len(values['filest'])==2)
    read_traces_and_evaluate(values)


if __name__ == "__main__":
    main(sys.argv[1:])
