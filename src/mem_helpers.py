import datetime
import dateutil
import dateparser
import re

default_zero = datetime.datetime(2020, 2, 24)

def id_to_strdate(delta, date_zero=default_zero):
    day=(date_zero + datetime.timedelta(delta))
    if not day.year==datetime.datetime.today().year:
        return day.strftime('%d %b %Y')
    else:
        return day.strftime('%d %b')

def id_to_isodate(delta, date_zero=default_zero):
    day=(date_zero + datetime.timedelta(delta))
    return day.isoformat()

def id_to_date(delta, date_zero=default_zero):
    return (date_zero + datetime.timedelta(delta))

def strdate_to_id(string, date_zero=default_zero):
    istring=re.compile('^\{?([^}]+)\}?$').match(string).groups()[0]
    return (dateutil.parser.parse(istring) - date_zero).days

def isodate_to_id(string, date_zero=default_zero):
    return (dateutil.parser.isoparse(string) - date_zero).days

def date_to_id(date, date_zero=default_zero):
    return (date-date_zero).days

def strdate_to_date(string):
    istring=re.compile('^\{?([^}]+)\}?$').match(string).groups()[0]
    return dateutil.parser.parse(istring)

def isodate_to_date(string):
    return dateutil.parser.isoparse(string)

def date_to_strdate(day):
    if not day.year==datetime.datetime.today().year:
        return day.strftime('%d %b %Y')
    else:
        return day.strftime('%d %b')
    
def date_to_isodate(day):
    return day.isoformat()

def is_strint(string):
    return re.match(r"[-+]?\d+(\.0*)?$", string) is not None

def is_strdate(string):
    istring=re.compile('^\{?([^}]+)\}?$').match(string).groups()[0]
    return dateutil.parser.parse(istring) is not None

def is_date(t):
    return isinstance(t, datetime.date) 
