#!/usr/bin/env python3

import datetime
import mem_helpers

import sys
import getopt
import re

from yaml import load as yamlload, dump as yamldump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import weibull_min



# Enrich a trace, by computing the "derived" parameters

# Methods:
# - Shift (of N days)
# - Weibull (with list coeff given)?
# - Weekly (branching depending on the day of the week) + 1 method per day
# - Custom (takes "t" + "trace" as parameter) => Specified in a file

# Get the day (Monday, ..., Sunday) of a week from t
def get_day_of_week(trace_start, t):
    date_start = mem_helpers.strdate_to_date(trace_start)
    date_t = date_start + datetime.timedelta(t)
    wday = date_t.weekday()
    return wday

def weekday_to_fieldname(day_week):
    assert(day_week>=0)
    assert(day_week<=6)
    if (day_week==0): return 'mon'
    elif (day_week==1): return 'tue'
    elif (day_week==2): return 'wed'
    elif (day_week==3): return 'thu'
    elif (day_week==4): return 'fri'
    elif (day_week==5): return 'sat'
    elif (day_week==6): return 'sun'

def subst_params(mparam, expr):
    # If no parameters
    if mparam is None:
        return expr

    if type(expr) is str:
        for param in mparam:
            expr = expr.replace(param, str(mparam[param]))
    return expr



# Perform the compuation specified by "work" for a given time step t
def enrich_iteration(mparam, work, trace, t):
    trace_t = trace['trace'][t]

    #print("work = " + str(work))
    #print("trace = " + str(trace))
    
    assert(len(work.items())==1)
    work_nature = None
    marg_work = None
    for (wn, marg) in work.items():
        work_nature = wn
        marg_work = marg

    # Matching start !!!
    if (work_nature=='shift'):
        # ('shift', {in_field: 'infected'; num_shift : 7; out_field = 'hospital'})
        in_field = marg_work['in_field']
        num_shift = marg_work['num_shift']
        out_field = marg_work['out_field']

        num_shift = subst_params(mparam, str(num_shift))
        num_shift = eval(num_shift)

        ntrace_t = {}
        for region in trace_t.keys():
            ntrace_t_reg = trace_t[region]
            if (t<num_shift):
                ntrace_t_reg[out_field] = 0.0
            else:
                ntrace_t_reg[out_field] = trace['trace'][t-num_shift][region][in_field]
            ntrace_t[region] = ntrace_t_reg
        return ntrace_t

    elif (work_nature=='convolution'):
        # ('convolution', {in_field : 'infected'; out_field = 'hospital'; wcoeff : [ ... ] })
        in_field = marg_work['in_field']
        out_field = marg_work['out_field']
        wcoeff = marg_work['wcoeff']

        # f_wcoeff = future weight coefficients
        if 'f_wcoeff' in marg_work.keys():
            f_wcoeff = marg_work['f_wcoeff']
        else:
            f_wcoeff = []

        wcoeff = subst_params(mparam, str(wcoeff))
        wcoeff = eval(wcoeff)
        if type(wcoeff) is list:
            wcoeff1 = []
            for x in wcoeff:
                if type(x) is str:
                    wcoeff1 = wcoeff1 + [eval(x)]
                else:
                    wcoeff1 = wcoeff1 + [x]
            wcoeff = wcoeff1
        wcoeff = [float(x) for x in wcoeff]

        f_wcoeff = subst_params(mparam, str(f_wcoeff))
        f_wcoeff = eval(f_wcoeff)
        if type(f_wcoeff) is list:
            f_wcoeff1 = []
            for x in f_wcoeff:
                if type(x) is str:
                    f_wcoeff1 = f_wcoeff1 + [eval(x)]
                else:
                    f_wcoeff1 = f_wcoeff1 + [x]
            f_wcoeff = f_wcoeff1
        f_wcoeff = [float(x) for x in f_wcoeff]
        
        #print(wcoeff)
        #print(f_wcoeff)
        
        # wcoeff: first one for t, last one for t-\tau
        ntrace_t = {}
        for region in trace_t.keys():
            ntrace_t_reg = trace_t[region]
            temp = 0.0
            for tau in range(0, min(len(wcoeff), t+1) ):
                temp += wcoeff[tau] * float(trace['trace'][t-tau][region][in_field])

            Tmax = len(trace['trace'])
            for tau in range(1, min(len(f_wcoeff), Tmax-t) ):
                assert(region != out_field), ("No self-dependence on " + out_field)
                temp += f_wcoeff[tau] * float(trace['trace'][t+tau][region][in_field])

            ntrace_t_reg[out_field] = temp
            ntrace_t[region] = ntrace_t_reg
        return ntrace_t

    elif (work_nature=='linear'):
        # ('linear', {out_field = 'hospital'; field_lcoeff : [ ... ] })
        out_field = marg_work['out_field']
        field_lcoeff = marg_work['field_lcoeff']
        
        n_trace_t = {}
        ntrace_t = {}
        for region in trace_t.keys():
            ntrace_t_reg = trace_t[region]
            temp = 0.0

            for (fname, lcoeff) in field_lcoeff.items():
                lcoeff = subst_params(mparam, str(lcoeff))
                lcoeff = eval(lcoeff)
                lcoeff = [float(x) for x in lcoeff]

                for tau in range(0, min(len(lcoeff), t+1) ):
                    if ((fname == out_field) and (tau==0)):
                        if lcoeff[tau] !=0:
                            raise ErrorCoeff("Self dependence in linear coeffs on " + out_field)
                    else:
                        temp += lcoeff[tau] * float(trace['trace'][t-tau][region][fname])

            ntrace_t_reg[out_field] = temp
            ntrace_t[region] = ntrace_t_reg
        return ntrace_t

    elif (work_nature=='weekly'):
        # ('weekly', { d1: work_Monday ; d2: work_Tuesday; d3: ... ; ... } )
        day_week = get_day_of_week(trace['start'], t)
        work_day = marg_work[weekday_to_fieldname(day_week)]
        return enrich_iteration(mparam, work_day, trace, t)

    elif (work_nature=='custom'):
        # ('custom', {out_field = 'hospital'; custom_code: ... })
        # In "custom_code" trace variable name is 'trace'
        custom_code = marg_work['custom_code']
        custom_code = subst_params(mparam, custom_code)
        ntrace_t = {}
        for region in trace_t.keys():
            ntrace_t_reg = trace_t[region]
            ntrace_t_reg[outfield] = eval(custom_code)
            ntrace_t[region] = ntrace_t_reg
        return ntrace_t
    else:
        raise Error("work_nature was not recognized !")

# Main logical function
def enrich_trace(trace, workyaml):
    lwork = workyaml['work']
    if 'param' in workyaml: mparam = workyaml['param']
    else: mparam=None

    # Loop over time
    Tend = len(trace['trace'])
    for work in lwork:
        for t in range(0, Tend):
            trace['trace'][t] = enrich_iteration(mparam, work, trace, t)
    return trace

# =================================================

def trace_yaml_structure(trace):
    return trace

# Write trace in file "file"
def write_trace(trace, file):
    yaml_struct = trace_yaml_structure(trace)
    ftrace = open(file, 'w')
    yamldump(yaml_struct, ftrace, default_flow_style=False)
    ftrace.close()


# I/O wrapper around the main function
def main_enrich_trace(values):
    file_work = values['fwork']
    work_input = values['yaml_work']

    lfile_itrace = values['itrace']
    litrace_input = values['yaml_itrace']

    lfile_otrace = values['otrace']

    if lfile_otrace is not None:
        assert(len(lfile_itrace)==len(lfile_otrace))

    #print("values = " + str(values))

    # Input management
    if (work_input==None):
        fwork = open(file_work, 'r')
        workyaml = yamlload(fwork, Loader=Loader)
        fwork.close()
    else:
        workyaml = work_input

    # DEBUG
    #print("lfile_itrace = " + str(lfile_itrace))

    ltrace = []
    if (litrace_input==None):
        for file_itrace in lfile_itrace:
            fitrace = open(file_itrace, 'r')
            yaml_itrace = yamlload(fitrace, Loader=Loader)
            trace = yaml_itrace
            fitrace.close()
            assert(yaml_itrace is not None)

            ltrace = ltrace + [yaml_itrace]
    else:
        ltrace = litrace_input

    assert(workyaml!=None)
    assert(ltrace!=None)

    # Main call
    lenriched_trace = []
    for trace in ltrace:
        enriched_trace = enrich_trace(trace, workyaml)
        lenriched_trace = lenriched_trace + [enriched_trace]

    #print("enriched_trace = " + str(enriched_trace))

    # Output management
    if lfile_otrace is not None:
        for i in range(0, len(lfile_otrace)):
            write_trace(lenriched_trace[i], lfile_otrace[i])
        return None
    else:
        lres = []
        for enriched_trace in lenriched_trace:
            res = trace_yaml_structure(enriched_trace)
            lres = lres + [res]
        return lres
    

# =================================================

default_values = {}
default_values['fwork'] = 'work_enrich.yaml'        # File name
default_values['yaml_work'] = None
default_values['itrace'] = ['/dev/stdin']  # File name
default_values['yaml_itrace'] = None
default_values['otrace'] = ['/dev/stdin'] # File name

def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[-i|--input= file] input traces (default STDIN)'+
          '\n\t[-o|--output= file] output traces'+
          '\n\t[-w|--work= file] input yaml file specifying the enrichment'
          )

def parse_opts_enrich(values, opts):
    for opt, arg in opts:                
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        elif opt == '-d':
            global _debug
            _debug = 1
        elif opt in ["-i", "--input"]:
            values['itrace'] = arg.split(',')
        elif opt in ["-o", "--output"]:
            values['otrace'] = arg.split(',')
        elif opt in ["-w", "--work"]:
            values['fwork'] = arg
    return values

def get_values_enrich(opts):
    return parse_opts_enrich(default_values, opts)

def main(argv):
    values = {}
    for (k,v) in default_values.items():
        values[k] = v

    try:                                
        opts, args = getopt.getopt(argv, "hi:o:w:", ["help", "input=", "output=", "work="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    values = parse_opts_enrich(values, opts)

    main_enrich_trace(values)

if __name__ == "__main__":
    main(sys.argv[1:])
