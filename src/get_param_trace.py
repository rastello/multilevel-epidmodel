


from mem_helpers import id_to_isodate
from parse_seepia import get_all_groupname


# Naming convention for parameters of a parametrized trace
def get_param_name(id):
    return ('__ptrace_' + str(id) + "__")

default_size_all = 63973538


# Generate a parametrized yaml trace - limitation: only for "all" as unique region
#  Windows of trace: [window_start; window_end[
def generate_parametrized_trace(window_start, window_len, param_field):
    all_groupname = get_all_groupname()
    trace_yaml = {}

    trace_yaml['rename'] = { all_groupname : 'France'}
    trace_yaml['start'] = id_to_isodate(window_start)

    ltrace = []
    for t in range(0, window_len):
        mtrace_reg_t = {}
        mtrace_reg_t[param_field] = get_param_name(t)
        mtrace_reg_t['size'] = default_size_all

        mtrace_t = {}
        mtrace_t[all_groupname] = mtrace_reg_t
        ltrace = ltrace + [mtrace_t]
    trace_yaml['trace'] = ltrace

    return trace_yaml

# Write trace in file "file"
def write_trace(trace, file):
    yaml_struct = trace_yaml_structure(trace)
    ftrace = open(file, 'w')
    yamldump(yaml_struct, ftrace, default_flow_style=False)
    ftrace.close()


# Given a mapping [param_name -> param_value], replace the param name by their values
def evaluate_trace(trace_yaml, mparam):
    for t in range(0, len(trace_yaml['trace'])):
        for region in trace_yaml['trace'][t]:
            for field in trace_yaml['trace'][t][region]:
                if type(trace_yaml['trace'][t][region][field]) is str:
                    tempstr = trace_yaml['trace'][t][region][field]
                    for param in mparam:
                        tempstr = tempstr.replace(param, str(mparam[param]))
                    trace_yaml['trace'][t][region][field] = int(tempstr)
    return trace_yaml




# =======================================

def main_generate_parametrized_trace(values):
    window_start = values['win_start']
    window_len = values['win_len']
    param_field = values['param_field']
    fwrite_trace = values['output']

    assert((window_start + window_len) < 0)

    if (values['mparam'] is None):
        stream = open(values['input_cfg'], 'r')
        mparam = yamlload(stream, Loader=Loader)
        stream.close()
    else:
        mparam = values['mparam']

    trace_yaml = generate_parametrized_trace(window_start, window_len, param_field)
    trace_yaml = evaluate_trace(trace_yaml, mparam)

    if (fwrite_trace is not None):
        write_trace(trace_yaml, fwrite_trace)
        return None
    else:
        return trace_yaml



default_values = {}
default_values['win_start'] = -50
default_values['win_len'] = 30
default_values['param_field'] = 'severity'
default_values['output'] = None
default_values['input_cfg'] = 'param_tuner.cfg'
default_values['mparam'] = None


def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[--wstart= int] start of the window'+
          '\n\t[--wlen= int] length of the window (wstart+wlen <= -1)'+
          '\n\t[--field= fieldname] name of the field filled with parameters'+
          '\n\t[--output= filename] output the produced trace'+
          '\n\t[--input_cfg= filename] input, values of the parameters'
          )

def parse_opts_paramtrace(values, opts):
    for opt, arg in opts:                
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        elif opt == '-d':
            global _debug
            _debug = 1
        elif opt == '--wstart':
            values['win_start']=int(arg)
        elif opt == '--wlen':
            values['win_len']=int(arg)
        elif opt == '--field':
            values['param_field'] = arg
        elif opt == '--output':
            values['output'] = arg
        elif opt == '--input_cfg':
            values['input_cfg'] = arg
    return values

def get_values_paramtrace(opts):
    return parse_opts_paramtrace(default_values, opts)


def main(argv):
    values = {}
    for (k,v) in default_values.items():
        values[k] = v

    try:                                
        opts, args = getopt.getopt(argv, "h", ["help", "wlen=", "field=", "output=", "input_cfg="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    values = parse_opts_paramtrace(values, opts)

    main_generate_parametrized_trace(values)

if __name__ == "__main__":
    main(sys.argv[1:])
