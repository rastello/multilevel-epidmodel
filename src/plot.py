#!/usr/bin/env python3

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU
from yaml import load as yamlload, dump as yamldump
import sys
import getopt
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import mem_helpers
import datetime
import matplotlib.dates as mdates
import numpy as np
import re
    
_debug=0

def read_trace_and_plot(filenames, aggregate=False, stats=['infected'], groupf=None, normalize=False, output=None, accumulate=False):
    fig, ax=plt.subplots()
    linestyle='solid'
    datemin=None
    datemax=None
    ngroups=0
    nstats=0
    color={}

    assert len(stats)==1 or aggregate is True, "mulitple stats should aggregate groups"
    for filename in filenames:
        stream=open(filename, 'r')
        memt=yamlload(stream, Loader=Loader)
        trace=memt['trace']
        rename=memt['rename']
        if 'start' in memt:
            start=mem_helpers.strdate_to_date(memt['start'])
        else: start=None
        for what in stats:
            if what not in color:
                color[what]='C{:d}'.format(nstats)
                nstats+=1
    
            groups=set()
            dates=[]
            plot={}
            ALL='-all'
            INT='-int'
    
            if what=='immuned':
                what='infected'
                accumulate=True
            elif what is None:
                what='infected'
            
            if normalize is True:
                size={}
            plot[what]={}
            if aggregate is True:
                plot[what+ALL]=[]
                if accumulate is True:
                    plot[what+INT+ALL]=[]
            elif accumulate is True:
                plot[what+INT]={}

            for t in range(0,len(trace)):
                if start is not None:
                    dates.append(start+datetime.timedelta(t))
                else: dates.append(t)

                if aggregate is True:
                    plot[what+ALL].append(0)
                    if accumulate is True: plot[what+INT+ALL].append(0)

                for G in trace[t]:
                    if groupf is not None:
                        if G not in groupf: continue
                    if G not in color:
                        color[G]='C{:d}'.format(ngroups)
                        ngroups+=1
                    if G not in groups:
                        assert(t==0)
                        groups.add(G)
                        if normalize is True:
                            size[G]=trace[0][G]['size']
                        plot[what][G]=[trace[0][G][what]]
                        if accumulate is True:
                            plot[what+INT][G]=[trace[0][G][what]]
                    else:
                        plot[what][G].append(trace[t][G][what])
                        if accumulate is True:
                            plot[what+INT][G].append(plot[what+INT][G][-1]+trace[t][G][what])
                    if aggregate is True:
                        plot[what+ALL][-1]+=plot[what][G][-1]
                        if accumulate is True:
                            plot[what+INT+ALL][-1]+=plot[what+INT][G][-1]

            if accumulate is True: what=what+INT
            if aggregate is True:
                if normalize is True:
                    size_total=sum(size)
                    plot[what+ALL]=[x/size_total for x in plot[what+ALL]]
                rename[what]=re.sub('_', '', what)
                ax.plot(dates, plot[what+ALL], label=rename[what], linestyle=linestyle, color=color[what])
            else:
                for G in groups:
                    if normalize is True:
                        plot[what][G]=[x/size[G] for x in plot[what][G]]
                    ax.plot(dates, plot[what][G], label=rename[G], linestyle=linestyle, color=color[G])
        stream.close()
            
        if start is not None:
            if datemin is None:
                datemin = np.datetime64(dates[0], 'W')
            else: datemin=min(datemin, np.datetime64(dates[0], 'W'))
            if datemax is None:
                datemax = np.datetime64(dates[-1], 'W') + np.timedelta64(1, 'W')
            else: datemax=max(datemax, np.datetime64(dates[-1], 'W') + np.timedelta64(1, 'W'))
        linestyle='dashed'

    
    ax.legend(loc='best')
    if start is not None:
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
        ax.xaxis.set_minor_locator(mdates.WeekdayLocator(byweekday=(SA, SU)))
        ax.set_xlim(datemin, datemax)
        fig.autofmt_xdate()
        plt.grid(b=True, axis='x', which='minor')
        plt.grid(b=True, axis='x', which='major', color='black')
        plt.grid(b=True, axis='y', which='major')

        
    if output is not None:
        fig.savefig(output, bbox_inches='tight')
    else: plt.show()
        

def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[-t|--trace= file[,file]]'+
          '\n\t[-a|--aggregate]'+
          '\n\t[-n|--normalize]'+
          '\n\t[-s|--stats= infected|immuned|susceptible|...]'+
          '\n\t[-g|--groups= group1,group2,...] restricts to those groups' +
          '\n\t[-o|--output= figname] exports plot'
          )

def main(argv):
    groupf=None
    filet=['/dev/stdin']
    aggregate=False
    what='infected'
    normalize=False
    output=None
    try:                                
        opts, args = getopt.getopt(argv, "ht:as:g:no:", ["help", "trace=", "aggregate", "stats=", "groups=", "normalize", "output="])
    except getopt.GetoptError:           
        usage()                          
        sys.exit(2)
    for opt, arg in opts:                
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        if opt in ("-t", "--trace"):
            filet=arg.split(',')
        elif opt in ("-g", "--groups"): 
            groupf=arg.split(',')
        if opt in ("-a", "--aggregate"):
            aggregate=True
        if opt in ("-n", "--normalize"):
            normalize=True
        if opt in ("-s", "--stats"):
            what=arg.split(',')
        if opt in ('-o', '--output'):
            output=arg
            
    read_trace_and_plot(filet, aggregate, what, groupf, normalize, output)


if __name__ == "__main__":
    main(sys.argv[1:])
