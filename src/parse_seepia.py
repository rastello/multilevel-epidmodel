#!/usr/bin/env python3

import csv
from scipy.stats import weibull_min
import sys
import getopt
import os
import re
import process_csv

from yaml import load as yamlload, dump as yamldump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import mem_helpers

_debug=False

def debug(d):
    global _debug
    _debug=d

alias={'all':'0_0', 'men':'1_0', 'women':'2_0', '15[':'0_A', '[15-45[':'0_B', '[45-65[':'0_C', '[65-75[':'0_D', '[75-':'0_E'}
actes={}
urgences={}
hospitalisations={}
deces={}
population={}
regions=set()
groupname={}
categories=set()
days=set()
stats = {'actes': actes, 'urgences' : urgences, 'hospitalisations':hospitalisations, 'deces':deces}
date_zero=None

def dump_stats(phase, category):
    for region in stats[phase]:
        print(region +  " population:" + str(population[region]) + " " + category + ":["  +  ' '.join(str(v) for k,v in sorted(stats[phase][region][alias[category]].items()))  +  "]")

# =========================

# Naming convention for the group "all"
def get_all_groupname():
    return 'RFR'


# Build the logical data structure that corresponds to the produced .memi file
def build_memi_struct(phase, t, N, Wshape=None, Wscale=None, regionf=None, per_region=True):
    if Wshape is None: Wshape=2.24
    if Wscale is None: Wscale=5.42
    
        
    T=len(stats[phase][next(iter(regions))][alias['all']])
    # delta with cur_date is the standard. 
    if mem_helpers.is_date(t): t=mem_helpers.date_to_id(t)-T

    Nname={}
    if type(N) is str and re.compile('(^|[^a-zA-Z])t($|\W)').search(N):
        for region in regions:
            if not regionf is None:
                if not region in regionf: continue
            if per_region:
                Nname[region]=f'N{region}(t)'
            else: Nname[region]='N(t)'
        
        # t in the simulator starts from 0...
        Tzero=mem_helpers.id_to_date(t+T+1, date_zero)
        def match_to_t(match):
            string=match.groups()[0]
            try:
                return str(mem_helpers.strdate_to_id(string, Tzero))
            except:
                sys.exit("Could not parse date "+string)
        N=re.compile('\{([^}]+)\}').sub(match_to_t, N)
    else:
        for region in regions:
            if not regionf is None:
                if not region in regionf: continue
            if per_region:
                Nname[region]=f'N{region}'
            else: Nname[region]='N'
    
    # Build the object that will be dumped by the yaml
    memiobj = {}

    metadata_memi = {'t': t,
                     'date': mem_helpers.id_to_isodate(t+T, date_zero),
                     'phase': phase }
    memiobj['metadata'] = metadata_memi

    parameters_memi = {'Wshape': Wshape,
                       'Wscale': Wscale }
    if per_region:
        for region in Nname:
            parameters_memi[Nname[region]] = N
    else: parameters_memi[Nname[next(iter(Nname))]] = N
    memiobj['parameters'] = parameters_memi

    vars_memi = {'_W': 'weibull(nb_time_steps=10, shape=parameters[\'Wshape\'], scale=parameters[\'Wscale\'])' }
    memiobj['vars'] = vars_memi

    groups_memi = {}
    for region in regions:
        if not regionf is None:
            if not region in regionf: continue
        
        size=population[region]
        region_groups_memi = {}
        region_groups_memi['long name'] = groupname[region]
        region_groups_memi['size'] = size
        region_groups_memi['contagiousity'] = '_W'  # Note: did not find way to build dumpable aliases
        groups_memi[region] = region_groups_memi
    memiobj['groups'] = groups_memi

    contacts_memi = {}
    for region in regions:
        if not regionf is None:
            if not region in regionf: continue
        #Note: difference: should have " " around the following string
        contacts_memi['{region} from {region}'.format(region=region)] = 'parameters[\'{N}\'] / groups[\'{region}\'][\'size\']'.format(region=region, N=Nname[region])
    memiobj['contacts'] = contacts_memi

    states_memi = {}
    for region in regions:
        if not regionf is None:
            if not region in regionf: continue
        size=population[region]
        infected=[x/size for x in list(stats[phase][region][alias['all']].values())]
        immuned=sum(infected)/size
        susceptible=1-immuned

        #Note: difference: should be something like {susceptible: 1.0, infected: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}
        # => No { } and no [] here
        state_reg_memi = {}
        state_reg_memi['susceptible'] = susceptible
        #temp_linfected = ', '.join(list(map('{:.5g}'.format,infected[t-10:t])))
        state_reg_memi['infected'] = infected[t-10:t] #temp_linfected
        states_memi[region] = state_reg_memi

    memiobj['states'] = states_memi
    return memiobj

# Build and write the memi
def write_memi_lockdown_all(memi_file, phase, t_date, N, Wshape=None, Wscale=None, regionf=None):
    memiobj = build_memi_struct(phase, t_date, N, Wshape, Wscale, regionf)

    # Output on a file
    ymlf = open(memi_file, "w")
    yamldump(memiobj, ymlf, default_flow_style=False)
    ymlf.close()

    return None


# Build the logical data structure that corresponds to the produced trace file
#TODO: use parameter stats to choose what is included in trace
def build_trace_struct(stats, window, regionf):
    size={}
    dead={}
    hospitalized={}
    assert(len(regions)>0)
    for region in regions:
        if not regionf is None:
            if not region in regionf: continue
        size[region]=int(population[region])
        if deces != {}:
            dead[region]=list(deces[region][alias['all']].values())
        hospitalized[region]=list(hospitalisations[region][alias['all']].values())
        S=size[region]
        T=len(hospitalized[region])

    # we go from window[0] excluded (to match with the simulator) to window[1] included
    if window is None:
        window=[-1,T-1]
    if mem_helpers.is_date(window[0]):
        window[0]=mem_helpers.date_to_id(window[0])
    if mem_helpers.is_date(window[1]):
        window[1]=mem_helpers.date_to_id(window[1])
    tmin=(window[0]+1) % T
    tmax=(window[1] % T) + 1


    yaml_trace = {}
    yaml_trace['start'] = mem_helpers.id_to_isodate(tmin)

    trace_yaml_trace = []
    for t in range(tmin,tmax):
        time_stamp_trace = {}

        total_size=0
        
        for region in regions:
            if not regionf is None:
                if not region in regionf: continue
            
            data_time_region = {}
            data_time_region['size'] = size[region]
            data_time_region['hospitalized'] = hospitalized[region][t]
            if deces != {}:
                data_time_region['dead'] = dead[region][t]
            time_stamp_trace[region] = data_time_region
        trace_yaml_trace = trace_yaml_trace + [time_stamp_trace]
    yaml_trace['trace'] = trace_yaml_trace

    rename_yaml_trace = {}
    for region in regions:
        if not regionf is None:
            if not region in regionf: continue
        rename_yaml_trace[region] = groupname[region]
    yaml_trace['rename'] = rename_yaml_trace

    return yaml_trace

# Build and right the trace file
def write_trace(trace_file, phase, window, regionf):
    yaml_trace = build_trace_struct(phase, window, regionf)

    tf=open(trace_file, "w")
    yamldump(yaml_trace, tf, default_flow_style=False)
    tf.close()

    return None

# =========================

def last_day(csvfile=None):
    if csvfile is None: csvfile=process_csv.in_file
    fields=None
    with open(csvfile) as f:
        for line in f:
            if fields is None:
                fields=line.rsplit(';')
            pass
        lastline = line.rsplit(';')
    for i, f in enumerate(fields):
        if f=='date':
            return mem_helpers.strdate_to_date(lastline[i])
    sys.exit('Could not parse {} file'.format(csvfile))

def parse_stats(csvstats):
    fields={'Region': 'région', 'Day': 'jour', 'Date': 'date', 'Sex': 'sexe', 'Age': 'sursaud_cl_age_corona', 'Actes': 'nbre_acte_corona', 'Urgences': 'nbre_pass_corona', 'Hospitalisations': 'nbre_hospit_corona', 'Population': 'population', 'Long name': 'région_long', 'Deces': 'nbre_deces'}
    #csvstats=process_csv.parse_csv(csvfile)
    global date_zero
    for row in csvstats:
        region='R' + str(row[fields['Region']])
        if region not in regions:
            regions.add(region)
            if fields['Long name'] in row:
                groupname[region] = row[fields['Long name']]
            else:
                groupname[region]='Region ' + str(row[fields['Region']])
            actes[region]={}
            urgences[region]={}
            hospitalisations[region]={}
            if fields['Deces'] in row:
                deces[region]={}
            population[region]=int(row[fields['Population']])
            for category in categories:
                actes[region][category]={}
                urgences[region][category]={}
                hospitalisations[region][category]={}
                if fields['Deces'] in row:
                    deces[region][category]={}
        category=row[fields['Sex']] + '_' + row[fields['Age']]
        if category not in categories:
            categories.add(category)
            actes[region][category]={}
            urgences[region][category]={}
            hospitalisations[region][category]={}
            if fields['Deces'] in row:
                deces[region][category]={}
        day=int(row[fields['Day']])
        if date_zero is None:
            date_zero=mem_helpers.id_to_date(-day, mem_helpers.isodate_to_date(row[fields['Date']]))
        actes[region][category][day] = int(row[fields['Actes']])
        urgences[region][category][day] = int(row[fields['Urgences']])
        hospitalisations[region][category][day] = int(row[fields['Hospitalisations']])
        if fields['Deces'] in row:
            deces[region][category][day]=int(row[fields['Deces']])

# =========================

# Default values
default_values = {}
default_values['regionf'] = None
default_values['event'] = 'hospitalisations'
default_values['N'] = 2.5
default_values['list-regions'] = False
default_values['csv'] = None
default_values['memi'] = None
default_values['trace'] = None
default_values['window'] = None
default_values['start'] = -1
default_values['all'] = False
default_values['Wshape'] = 2.24
default_values['Wscale'] = 5.42
default_values['metaregions-file']=None
default_values['merge-regions']=False
default_values['merge-age']=False

def main_parse_seepia(values_param):
    if _debug: print(f'parse_seepia.main_parse_seepia({values_param})')
    for param in default_values:
        if param not in values_param:
            values_param[param]=default_values[param]
    regionf= values_param['regionf']
    event= values_param['event']
    N= values_param['N']
    list_regions= values_param['list-regions']
    csvfile = values_param['csv']
    memi_file= values_param['memi']
    trace_file= values_param['trace']
    window= values_param['window']
    start = values_param['start']
    Wshape= values_param['Wshape']
    Wscale= values_param['Wscale']
    metareg_file = values_param['metaregions-file']
    do_regions = values_param['merge-regions']
    merge_all_regions = values_param['all']
    do_age = values_param['merge-age'];

    tbl = process_csv.parse_csv(csvfile)
    if merge_all_regions is True: tbl = process_csv.merge_allregions(tbl) 
    elif do_regions: tbl = process_csv.merge_regions(tbl, metareg_file)
    if do_age == True: tbl = process_csv.merge_age(tbl)
    parse_stats(tbl)

    if list_regions==True:
        print(regions)
        sys.exit()
    
    if not memi_file is None:
        memiobj = write_memi_lockdown_all(memi_file, event, start, N, Wshape, Wscale, regionf)
    else:
        memiobj = build_memi_struct(event, start, N, Wshape, Wscale, regionf)

    if not trace_file is None:
        traceobj = write_trace(trace_file, event, window, regionf)
    else:
        traceobj = build_trace_struct(event, window, regionf)
    return (memiobj, traceobj)
 
def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[-i|--csv= file] input csv file'+
          '\n\t[-N N] fixes parameter N'+
          '\n\t[--Wshape k] fixes shape parameter of Weibull distribution used for contagiosity (default 5.42)'+
          '\n\t[--Wscale lambda] fixes scale parameter of Weibull distribution used for contagiosity (default 2.24)'+
          '\n\t[-e|--event= actes|urgences|hospitalisations] uses this event (default is hospitalisations)'+
          '\n\t[-l|--list-regions] list regions'+
          '\n\t[--memi= file] creates memi file for simulator'+
          '\n\t[-s|--start t|date] + timestamp for memi file. Default is t=-1'+  
          '\n\t[-t|--trace= file] creates trace file'+
          '\n\t[-w|--window [t[,dt]] trace window {i in t<i<=t+dt<=-1}. Default is {i s.t. i<=-1}. If dt is ommited {i s.t t<i<=-1}. /!\\ -1=today; -2==yesterday... /!\\'+
          '\n\t[-w|--window [{date}[,dt]] trace window {i in {date}<i<=t+dt<=-1}. If dt is ommited {i s.t {date}<i<=-1}. '+
          '\n\t[-w|--window [{date},{date\'}] trace window {i in {date}<i<={date\'}}'+
          '\n\t[-r|--region= region1,region2,...] restricts to those regions'+
          '\n\t[-a|--all] merge all regions together'+
          '\n\t[--merge-regions] merge regions as specified in metaregions-file'+
          '\n\t[--merge-age] merge age categories'+
          '\n\t[--metaregions-file= file')
          

def parse_opts_seepia(values, opts):
    for opt, arg in opts:
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        elif opt == '-d':                
            global _debug               
            _debug = True                  
        elif opt in ("-r", "--regions"): 
            values['regionf']=arg.split(',')
        elif opt in ("-a", "--all"):
            values['all']=True
        elif opt in ['--metaregions-file']:
            values['metaregion-file']= arg
        elif opt in ['--merge-regions']:
            values['merge-regions'] = True
        elif opt in ['--merge-age']:
            values['merge-age'] = True;
        elif opt in ("-i", "--csv"):
            values['csv']=arg
        elif opt in ("-N"):
            values['N']=arg
        elif opt in ("--Wshape"):
            values['Wshape']=float(arg)
        elif opt in ("-Wscale"):
            values['Wscale']=float(arg)
        elif opt in ("-e", "--event"):
            values['event']=arg
            assert(values['event'] in stats)
        elif opt in ("-l", "--list-regions"):
            values['list-regions']=True
        elif opt in  ("--memi"):
            values['memi']=arg
        elif opt in ("-t", "--trace"):
            values['trace']=arg
        elif opt in ("-w", "--window"):
            values['window']=[x for x in arg.split(',')]
            if mem_helpers.is_strint(values['window'][0]):
                values['window'][0]=int(values['window'][0])
                assert values['window'][0]<-1, 'empty window interval'
            elif mem_helpers.is_strdate(values['window'][0]):
                values['window'][0]=mem_helpers.strdate_to_date(values['window'][0])
                
            if len(values['window'])==1:
                values['window'].append(-1)
            else:
                if mem_helpers.is_strint(values['window'][1]):
                    values['window'][1]=int(values['window'][1])
                    if isinstance(values['window'][0], int):
                        values['window'][1]+=values['window'][0]
                        assert values['window'][1]<0, 'window interval goes beyond today. {}<=-1'.format(str(values['window'][1]))
                    else:
                        values['window'][1]=mem_helpers.id_to_date(values['window'][1], values['window'][0])
                elif mem_helpers.is_strdate(values['window'][1]):
                    values['window'][1]=mem_helpers.strdate_to_date(values['window'][1])
                else: assert(False), 'unrocognized format for window '+arg
                    
        elif opt in ("-s" , "--start"):
            if mem_helpers.is_strint(arg):
                values['start']=int(arg)
                assert(values['start']<0)
            elif mem_helpers.is_strdate(arg):
                values['start']=mem_helpers.strdate_to_date(arg)
    return values

def get_values_seepia(opts):
    if _debug: print(f'parse_seepia.get_values_seepia({opts})')
    return parse_opts_seepia(default_values, opts)


def main(argv):
    values = {}
    for (k,v) in default_values.items():
        values[k] = v

    try:                     
        opts, args = getopt.getopt(argv, "hi:r:e:ls:t:dN:w:a", ["help", "csv=", "regions=", "event=", "list-regions", "start=", "memi=", "trace=", "all", "window=", "Wshape=", "Wscale=", 'merge-regions', 'metaregions-file=', 'merge-age']) 
    except getopt.GetoptError:           
        usage()                          
        sys.exit(2)

    values = parse_opts_seepia(values, opts)
    main_parse_seepia(values)

if __name__ == "__main__":
    main(sys.argv[1:])
