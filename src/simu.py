#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import weibull_min
from yaml import load as yamlload, dump as yamldump
import re
import sys
import getopt
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import mem_helpers

# Naming convention for "all"
from parse_seepia import get_all_groupname

_debug=0


def debug(d):
    global _debug
    _debug=d

def weibull(nb_time_steps=10, shape=2.24, scale=5.42):
    return weibull_min.pdf([*range(0,nb_time_steps)], shape, 0, scale)


class Groups:
    # Create a population made up of groups
    #   Once initialized groups must be append (self.append(G)).
    #   Once all states of each group G is set (G.set_state(...)), population state must be updated (poulation.update())
    #   population.update() must be done at every time stamp once newly infected ratio have been computed for each group
    def __init__(self, metadata=None):
        self.metadata=metadata
        self.list=[]
        self.group={}
        self.size=0
        self.infected=[0]
        self.susceptible=[0]
        self.immuned=[0]
        self.t=0
        self.log=[]
        self.log.append({})
        self.memi=None

    def append(self, G):
        self.list.append(G)
        self.group[G.name]          = G
        self.size                  += G.size
        return G

    def update_state(self, G):
        self.infected[0]      += G.infected[-1] * G.size
        self.susceptible[0]   += G.susceptible * G.size
        self.immuned[0]       += G.immuned * G.size
        self.log[0][G.name]={}
        self.log[0][G.name]['size']=G.size
        self.log[0][G.name]['susceptible']=G.susceptible*G.size
        self.log[0][G.name]['infected']=G.infected[-1]*G.size
    
    def update(self):
        self.t+=1
        self.infected.append(0)
        self.susceptible.append(0)
        self.immuned.append(0)
        self.log.append({})
        for C in self.list:
            C.update()
            assert(C.t==self.t)
            self.infected[self.t]    += C.infected[-1]*C.size
            self.susceptible[self.t] += C.susceptible*C.size
            self.immuned[self.t]     += C.immuned*C.size
            self.log[self.t][C.name]={}
            self.log[self.t][C.name]['size']=C.size
            self.log[self.t][C.name]['susceptible']=int(C.susceptible*C.size+0.5)
            self.log[self.t][C.name]['infected']=int(C.infected[-1]*C.size+0.5)
        if self.memi is not None: self.update_contacts(self.t)

    def update_contacts(self, t):
        parameters=self.memi['parameters']
        parameters_t=self.memi['parameters_t']
        for param in parameters_t:
            parameters[param]=eval(parameters_t[param])

        groups=self.memi['groups']
    
        contacts=self.memi['contacts']
        contacts_t=self.memi['contacts_t']
        regex_econtact = re.compile('^(?P<dst>[^\s]+) from (?P<src>[^ ]+)$')
        for econtact in contacts_t:
            m=regex_econtact.match(econtact)
            G=self.group[m.group('dst')]
            Gfrom=self.group[m.group('src')]
            strength=contacts_t[econtact]
            G.contactfrom[Gfrom]=eval(strength)

    def plot_infected(self):
        T=[*range(0,self.t+1)]
        I=self.infected
        plt.plot(T, I)
        plt.show()

    def simulate(self, Tend):
        for t in range(self.t+1,Tend+1):
            for C in self.list:
                product=1
                for B in C.contactfrom:
                    for tau in range(1,10):
                        p_tau_B=min(1.0, float(C.contactfrom[B])*float(B.contagious[tau]))
                        IF_tau_B=B.infected[-tau]*B.size
                        product*=(1-p_tau_B)**IF_tau_B
                        #print('{C} {B} p_tau={ptau:.2g} IF_tau={IF:.2g} prod={prod:.2g}'.format(C=C.name, B=B.name, ptau=p_tau_B, IF=IF_tau_B, prod=product))
                nb_infected=(1-C.immuned)*Delta(1-product)
                C.set_infected(nb_infected)
            self.update()
            if not self.infected[-2]==0 and _debug>1: print('R:', self.infected[-1]/self.infected[-2])

    def write_trace(self, file, all=False):
        yaml_struct = self.trace_yaml_structure(all)
        trace = open(file, 'w')
        yamldump(yaml_struct, trace, default_flow_style=False)
        trace.close()

    # Generate the yaml data structure logically (cf write_trace) and return it
    def trace_yaml_structure(self, all=False):
        yaml_struct = {}
        if self.metadata is not None and self.metadata['date'] is not None:
            yaml_struct["start"] = mem_helpers.id_to_isodate(1,self.metadata['date'])

        ltime = []
        all_groupname = get_all_groupname()
        for t in range(1,self.t+1):
            mgroups = {}
            if all is False:
                for group in self.log[t]:
                    mvalues = {}
                    mvalues['size'] = self.log[t][group]['size']
                    mvalues['susceptible'] = self.log[t][group]['susceptible']
                    mvalues['infected'] = self.log[t][group]['infected']
                    mgroups[group] = mvalues
            else:
                mvalues = {}
                mvalues['size'] = self.size
                mvalues['susceptible'] = int(self.susceptible[t]+0.5)
                mvalues['infected'] = int(self.infected[t]+0.5)
                mgroups[all_groupname] = mvalues
            ltime = ltime + [mgroups]
        yaml_struct['trace'] = ltime

        rename_yaml_struct = {}
        if all is False:
            for group in self.log[t]:
                rename_yaml_struct[group] = self.group[group].longname
        else:
            rename_yaml_struct[all_groupname] = 'France'
        yaml_struct['rename'] = rename_yaml_struct
        return yaml_struct


    def __str__(self):
        return('\n'.join([str(C) for C in self.list]))

class Group:
    def __init__(self, population, name, size, contagious_law, longname=None):
        self.population=population
        self.name=name
        if longname is None: self.longname=name
        else: self.longname=longname
        self.size=size                             # integer>0
        assert(self.size>0)
        self.contagious=contagious_law             # distribution law from last 9 days
        self.contactfrom={}                        # level of contact from group B self.contactfrom[B]
        self.t=None
        self.next_infected=None        

    def set_state(self, infected=[0]*9, susceptible=None, immuned=None):
        self.infected=infected                     # infected[-9:-1]: amount of infected individuals on days [t-8..t]. 0 <= self.infected[t] <= 1
        assert(0<=min(self.infected) and max(self.infected)<=1)
        if immuned is None:
            self.immuned=sum(self.infected)     # amount of immuned individuals at time t 0 <= self.immuned[t] <=1
        else: self.immuned=immuned
        assert(0<=self.immuned<=1)
        if susceptible is None:                    # amount of susceptible individuals at time t 0 <= self.susceptible[t] <=1
            self.susceptible=1-self.immuned
        else: self.susceptible=susceptible
        assert(abs(self.immuned+self.susceptible-1)<1e-9)
        self.t=0
        self.population.update_state(self)
  
    def set_infected(self, x):
        assert(self.next_infected is None)
        self.next_infected=x

    def update(self):
        self.immuned+=self.next_infected
        self.susceptible-=self.next_infected
        self.infected=self.infected[1:]+[self.next_infected]
        self.t+=1
        self.next_infected=None
        
    def contacts(self):
        return keys(self.contactfrom)

    def __str__(self):
        return "{name} at time {time}: size={size} susceptibles={susceptibles} infected=[{infected}] immuned={immuned} contagious=[{contagious}]".format(name=self.name, time=self.t, size=self.size, susceptibles=self.susceptible, infected=' '.join(list(map('{:.2g}'.format,self.infected))), immuned=self.immuned, contagious=' '.join(list(map('{:.2g}'.format,self.contagious))))

def Delta(p):
    return p


# =====================================================================
# =====================================================================
# =====================================================================

def read_memi(filename, option_params={}):
    stream=open(filename, 'r')
    memi=yamlload(stream, Loader=Loader)
    stream.close()
    return memi
    
def memi_to_Groups(memi, option_params={}):
    #print(memi)
    regex_t = re.compile('(^|[^a-zA-Z])t($|\W)')
    t=0
    memi['parameters_t']= {}
    memi['contacts_t'] = {}
    depends_on_t=False

    if 'vars' in memi:
        vars=memi['vars']
    else: vars=[]
    
    if 'metadata' in memi:
        metadata=memi['metadata']
        population=Groups({'date': mem_helpers.strdate_to_date(metadata['date']), 'phase': metadata['phase']})
    else: population=Groups()
    regex_econtact = re.compile('^(?P<dst>[^\s]+) from (?P<src>[^ ]+)$')

    parameters=memi['parameters']
    for param in parameters:
        if param in option_params:
            parameters[param]=option_params[param]
        if type(parameters[param]) is str:
            for var in vars:
                parameters[param]=re.sub(var, vars[var], parameters[param])
            if regex_t.search(parameters[param]):
                depends_on_t=True
                memi['parameters_t'][param]=parameters[param]
            parameters[param]=eval(parameters[param])
    
    groups=memi['groups']
    group_simuobject={}
    for name in groups:
        size=groups[name]['size']
        if 'long name' in groups[name]:
            longname=groups[name]['long name']
        else: longname=name
        contagious_law=None
        if 'contagiousity' in groups[name]:
            if type(groups[name]['contagiousity']) is str:
                for var in vars:
                    groups[name]['contagiousity']=re.sub(var, vars[var], groups[name]['contagiousity'])
            contagious_law = groups[name]['contagiousity']
            if type(contagious_law) is str:
                contagious_law=eval(contagious_law)
            else: contagious_law=contagious_law
        G=population.append(Group(population, name, size, contagious_law, longname))
        group_simuobject[name]=G
            
    contacts=memi['contacts']
    for econtact in contacts:
        m=regex_econtact.match(econtact)
        if m:
            G=group_simuobject[m.group('dst')]
            Gfrom=group_simuobject[m.group('src')]
            if type(contacts[econtact]) is str:
                for var in vars:
                    contacts[econtact]=re.sub(var, vars[var], contacts[econtact])
                if regex_t.search(contacts[econtact]):
                    depends_on_t=True
                    memi['contacts_t'][econtact]=contacts[econtact]
                else:
                    if _debug>1:
                        sys.stderr.write(contacts[econtact])
            strength=contacts[econtact]
            if type(strength) is str:
                G.contactfrom[Gfrom]=eval(strength)
            else:
                G.contactfrom[Gfrom]=strength
        else: print("Error parsing contact {}".format(econtact))
        
    states=memi['states']
    for group in states:
        G=group_simuobject[group]
        susceptible = states[group]['susceptible']
        infected = states[group]['infected']
        G.set_state(infected, susceptible, 1-susceptible)

    if depends_on_t is True:
        population.memi=memi
    if _debug>2: print(population)
    return population

        
# =====================================================================
# =====================================================================
# =====================================================================

# Default values
default_values = {}
default_values['inputf'] = '/dev/stdin'
default_values['memi_input'] = None
default_values['option_params'] = {}
default_values['Tend'] = 50
default_values['plot'] = False
default_values['tracef'] = None
default_values['all'] = False

def main_simulator(values_param):
    if _debug>0: print(f'simu.main_simulator({values_param})')
    inputf = values_param['inputf']
    memi_input = values_param['memi_input']
    option_params = values_param['option_params']
    Tend = values_param['Tend']
    plot = values_param['plot']
    tracef = values_param['tracef']
    traceall = values_param['all']
    
    if (memi_input is None):
        memi=read_memi(inputf)
    else:
        memi = memi_input

    population=memi_to_Groups(memi, option_params)
    population.simulate(Tend)
    if plot==True: population.plot_infected()

    res = None
    if tracef is not None:
        population.write_trace(tracef, traceall)
    else:
        res = population.trace_yaml_structure(traceall)
    return res

def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[-i|--input= file] input file (default STDIN)'+
          '\n\t[-o|--trace= file]'+
          '\n\t[-p|--plot] plots'+
          '\n\t[-t t] + number of timestamps for the simulator. Default is 50'
          )

def parse_opts_simu(values, opts):
    for opt, arg in opts:                
        if opt in ("-h", "--help"):      
            usage()                     
            sys.exit()                  
        elif opt == '-d':
            global _debug
            _debug = 1
        elif opt == '-t':
            values['Tend']=int(arg)
        elif opt in ["-o", "--trace"]:
            values['tracef'] = arg
        elif opt in ["-a", "--all"]:
            values['all']=True
        elif opt in ["-i", "--input"]:
            values['inputf']=arg
        elif opt in ['-p', '--plot']:
            values['plot']=True
        elif opt in ['--param']:
            param,value = arg.split(':')
            values['option_params'][param]=value
    return values

def get_values_simu(opts):
    return parse_opts_simu(default_values, opts)

def main(argv):
    values = {}
    for (k,v) in default_values.items():
        values[k] = v

    try:                                
        opts, args = getopt.getopt(argv, "hi:o:apdt:",
            ["help", "input=", "trace=", "all", "plot", "param="]
            )
    except getopt.GetoptError:           
        usage()                          
        sys.exit(2)

    values = parse_opts_simu(values, opts)
    main_simulator(values)


if __name__ == "__main__":
    main(sys.argv[1:])
