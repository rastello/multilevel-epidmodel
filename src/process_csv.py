#
# Takes data from SEEPIA and groups regions together
# 

import sys
import os
import getopt
import csv
from yaml import load as yamlload, dump as yamldump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

categories = [(sex, '0') for sex in ['0', '1', '2']] + [('0', age) for age in ['A', 'B', 'C', 'D', 'E']]

def parse_csv(filename=None, use_sivic = False, sivic_filename=None):
    global in_file
    if filename is None: filename=in_file
    if sivic_filename is None: sivic_filename=sivic_file

    if use_sivic:
        deces = {}
        hospit_survenue = {}
        longnames = {}
        with open(sivic_filename) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',')
            for row in reader:
                reg = int(row['reg'])
                date = row['date']
                if reg not in deces:
                    deces[reg] = {}
                    hospit_survenue[reg] = {}
                    longnames[reg] = row['nom']
                if row['statut'] == 'Décès':
                    deces[reg][date] = int(row['survenue_entrees'])
                if row['statut'] == 'Toutes hospitalisations':
                    hospit_survenue[reg][date] = int(row['survenue_entrees'])


    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
        data = []
        for row in reader:
            nrow = {}
            reg = int(row['région'])
            date = row['date']
            if use_sivic and (date not in deces[reg] or row['sursaud_cl_age_corona'] != '0' or row['sexe'] != '0'): #SIVIC data begins a week later than SEEPIA and does not have subcategories
                continue
            for key, val in row.items():
                if key not in ['date', 'sexe', 'sursaud_cl_age_corona', 'région_long']:
                    nrow[key] = int(val)
                else:
                    nrow[key] = val
            if use_sivic:
                nrow['région_long'] = longnames[reg]
                nrow['nbre_deces'] = deces[reg][date]
                nrow['nbre_hospit_survenue'] = hospit_survenue[reg][date]
            data.append(nrow)
    return data

def write_csv(tbl, filename):
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=tbl[0].keys(), delimiter=';', quotechar='|')
        writer.writeheader()
        for row in tbl:
            writer.writerow(row)


def merge_age(tbl, metaage=None):
    if metaage is None:
        metaage = {'0': ['0'], 'ABC': ['A', 'B', 'C'], 'DE' : ['D', 'E']}

    rev_meta = {}
    for mage, ages in metaage.items():
        for age in ages:
            rev_meta[age] = mage
    newtbl = []
    day = '0'
    curr = {} # merged categories for current day
    
    for row in tbl:
        if row['jour'] != day:
            day = row['jour']
            for nrows in curr.values():
                for nrow in nrows.values():
                    newtbl.append(nrow)
            curr = {}

        cat = (row['sexe'], row['sursaud_cl_age_corona'])
        mage = rev_meta[row['sursaud_cl_age_corona']]
        mcat = (row['sexe'], mage)
        reg = row['région']
        if cat in categories:
            if reg not in curr:
                curr[reg] = {}
            if mcat not in curr[reg]:
                curr[reg][mcat] = dict(row)
                curr[reg][mcat]['sursaud_cl_age_corona'] = mage
            else:
                for key in ['nbre_acte_corona','nbre_pass_corona','nbre_hospit_corona','nb_pos','surface','population']:
                    curr[reg][mcat][key] += row[key]

    for nrows in curr.values():
        for nrow in nrows.values():
            newtbl.append(nrow)
    
    return newtbl

def merge_allregions(tbl, metaregions=None):
    global metafrance_file
    if metaregions is None: metaregions=metafrance_file
    return merge_regions(tbl, metaregions)
                     
def merge_regions(tbl, metaregions=None):
    if metaregions is None:
        metaregions=metareg_file
    if type(metaregions) is str:
        with open(metaregions) as f:
            metaregions = yamlload(f, Loader=Loader)

    rev_meta = {}
    for mreg, d in metaregions.items():
        for reg in d['subregions']:
            rev_meta[reg] = mreg
    newtbl = []
    day = '0'
    curr = {} # merged regions for current day
    
    for row in tbl:
        if row['jour'] != day:
            day = row['jour']
            for nrows in curr.values():
                for nrow in nrows.values():
                    newtbl.append(nrow)
            curr = {}

        cat = (row['sexe'], row['sursaud_cl_age_corona'])
        if row['région'] not in rev_meta:
            continue
        mreg = rev_meta[row['région']]
        if cat in categories:
            if mreg not in curr:
                curr[mreg] = {}
            if cat not in curr[mreg]:
                curr[mreg][cat] = dict(row)
                curr[mreg][cat]['région'] = mreg
                curr[mreg][cat]['région_long'] = metaregions[mreg]['longname']
            else:
                for key in ['nbre_acte_corona','nbre_pass_corona','nbre_hospit_corona','nb_pos','surface','population']:
                    curr[mreg][cat][key] += row[key]

    for nrows in curr.values():
        for nrow in nrows.values():
            newtbl.append(nrow)
    
    return newtbl

SEEPIAPATH = os.environ.get('SEEPIAPATH')
in_file = SEEPIAPATH+'/seepia-reg.csv' if not SEEPIAPATH is None else None
sivic_file = SEEPIAPATH+'/sivic_panel_reg_latest.csv'
out_file = SEEPIAPATH+'/seepia-merged.csv' if not SEEPIAPATH is None else None
CONFIGPATH = os.environ.get('CONFIGPATH')
metareg_file = CONFIGPATH+'/perdensity_metaregions.yaml' if not CONFIGPATH is None else None
metafrance_file = CONFIGPATH+'/france_metaregions.yaml' if not CONFIGPATH is None else None
use_sivic = False

def usage():
    print('Usage: '+sys.argv[0]+
          ' \n\t[-h|--help] dumps this help and exists'+
          '\n\t[-i|--input= file] input csv file (default seepia-reg.csv)'+
          '\n\t[-o|--output= file] output csv file (default seepia-merged.csv)'+
          '\n\t[--use-sivic]'+
          '\n\t[--merge-regions]'+
          '\n\t[--merge-age]'+
          '\n\t[--metaregions-file= file (default metaregions.yaml)')

def main(argv):
    global in_file, sivic_file, out_file, metareg_file, metafrance_file
    do_regions = False
    do_age = False

    try:                     
        opts, args = getopt.getopt(sys.argv[1:], "i:o:", ['input=', 'output=', 'merge-regions', 'metaregions-file=', 'merge-age', 'use-sivic']) 
    except getopt.GetoptError:           
        usage()                          
        sys.exit(2)
    for opt, arg in opts:
        if opt in ['-i', '--input']:
            in_file = arg
        elif opt in ['-o', '--output']:
            out_file = arg
        elif opt in ['--metaregions-file']:
            metareg_file = arg
        elif opt in ['--merge-regions']:
            do_regions = True
        elif opt in ['--merge-age']:
            do_age = True
        elif opt in ['--use-sivic']:
            use_sivic = True

    tbl = parse_csv(in_file, use_sivic)
    out_tbl = tbl
    if do_regions:
        with open(metareg_file) as f:
            metaregions = yamlload(f, Loader=Loader)
        out_tbl = merge_regions(out_tbl, metaregions)

    if do_age == True:
        metaage = {'0': ['0'], 'ABC': ['A', 'B', 'C'], 'DE' : ['D', 'E']}
        out_tbl = merge_age(out_tbl, metaage)

    assert((use_sivic or do_age or do_regions) is True), "Need to do something..."
    write_csv(out_tbl, out_file)


if __name__ == "__main__":
    main(sys.argv[1:])
