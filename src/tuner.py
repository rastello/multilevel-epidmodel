#!/usr/bin/env python3

import re
import subprocess
import sys

import numpy as np
import argparse

from yaml import load as yamlload, dump as yamldump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import opentuner
from opentuner import ConfigurationManipulator
from opentuner import IntegerParameter
from opentuner import FloatParameter
from opentuner import MeasurementInterface
from opentuner import Result

from simu import *
from error import evaluate, get_values_error_script
from parse_seepia import parse_stats, main_parse_seepia, get_values_seepia, last_day
from enrich import main_enrich_trace, get_values_enrich
from get_param_trace import *

from parse_seepia import debug as parse_seepia_debug
from simu import debug as simu_debug

_debug = 0

def debug(d):
    global _debug
    _debug=d

# Build the Python command script from the param.search
# For example: 
#     python3 -m parse_seepia --memi=temp_tuner_memi.memi --event=urgences -s -10 --all
# From:
#  - parse_seepia : {
#     memi : temp_tuner_memi.memi,
#     event : urgences,
#     s : -10,
#     all: True }
def get_python_command(mcommand):
    assert(len(mcommand)==1)
    com = ''
    opts_com = []
    for (script_name, margs) in mcommand.items():
        com = 'python3 -m ' + script_name
        for (argname, val) in margs.items():
            if (len(argname)==1):
                prefix = "-"
                eq_str = " "
            else:
                prefix = "--"
                eq_str = "="
            if type(val) is not list: val=[val]
            for v in val:
                if type(v) is bool:
                    if (v):
                        str_val = ""
                        eq_str = ""
                    else:
                        continue    # Option not activated
                else:
                    str_val = str(v)
                com += f' {prefix}{argname}{eq_str}{str_val!r}'
                opts_com = opts_com + [(f'{prefix}{argname}', str_val)]
        # DEBUG
        #print("COMMAND = " + com)
        #print("COMMAND OPTIONS = " + str(opts_com))
    return (com, opts_com)

# Check for unicity of "cmd_name" command & get it
def get_unique_cmd(cmd_run, cmd_name):
    cmd_simu = None
    for mcmd in cmd_run:
        assert(len(mcmd)==1)
        for (v,k) in mcmd.items():
            if (v==cmd_name):
                assert(cmd_simu==None)
                cmd_simu = mcmd
    return cmd_simu

def write_config_file(file_name, cfg):
    yaml_cfg = {}
    for param in cfg:
        yaml_cfg[param] = cfg[param]
    fcfg = open(file_name, 'w')
    yamldump(yaml_cfg, fcfg, default_flow_style=False)
    fcfg.close()



class SimuTuner(MeasurementInterface):
    def __init__(self, args, configfile='param.search', windows=None):
        config=open(configfile, 'r')
        search_param_yaml=yamlload(config, Loader=Loader)
        config.close()
        if _debug>0: print(f'tuner({args}, {windows})') 

        self.search_param=search_param_yaml['search_parameters']
        self.bcreate_file = search_param_yaml['create_file']
        if 'init' in search_param_yaml:
            self.cmd_csv2input= search_param_yaml['init']
        else: self.cmd_csv2input=None
        self.cmd_run=search_param_yaml['run']
        self.set_commands(windows)
        MeasurementInterface.__init__(self, args)

    def set_commands(self, windows):
        if windows is not None:
            assert self.cmd_csv2input is not None
            cmd_parse_seepia=get_unique_cmd(self.cmd_csv2input, 'parse_seepia')
            assert cmd_parse_seepia is not None, 'slidingwindow needs parse_seepia field'
            cmd_parse_seepia['parse_seepia']['s']=windows[0]
            cmd_parse_seepia['parse_seepia']['window']='{},{}'.format(windows[0], windows[1])
                
            cmd_param_trace = get_unique_cmd(self.cmd_run, 'get_param_trace')
            if cmd_param_trace is not None:
                cmd_param_trace['get_param_trace']['wstart']=windows[0]
                cmd_param_trace['get_param_trace']['wlen']=windows[1]

            cmd_simu = get_unique_cmd(self.cmd_run, 'simu')
            if cmd_simu is not None:
                cmd_simu['simu']['t']=windows[1]
                
        if self.cmd_csv2input is not None:
            (com, com_opts) = get_python_command(self.cmd_csv2input[0])
            if self.bcreate_file:
                if _debug>0: print(com)
                subprocess.getoutput(com)
            else:
                # Do it without file creation
                values_csv2input = get_values_seepia(com_opts)
                values_csv2input['memi'] = None
                values_csv2input['trace'] = None

                (memi_obj, trace_orig_obj) = main_parse_seepia(values_csv2input)

                self.memi_obj = memi_obj
                self.trace_orig_obj = trace_orig_obj
                assert(self.memi_obj is not None)
                assert(self.trace_orig_obj is not None)
        else:
            self.memi_obj=None
            self.trace_orig_obj=None


        # work_enrich : load logical struct & keep it across iterations
        cmd_enrich = get_unique_cmd(self.cmd_run, 'enrich')
        if cmd_enrich is not None:
            file_work_enrich = cmd_enrich['enrich']['work']

            fwork_enrich = open(file_work_enrich, 'r')
            self.work_enrich = yamlload(fwork_enrich, Loader=Loader)
            fwork_enrich.close()
        else:
            self.work_enrich = None

        self.score = -1.0

    
    # Search space definition
    def manipulator(self):
        manipulator = ConfigurationManipulator()
        # Note: We can have no parameters in search_param (comes from parametrized trace)
        if self.search_param is not None:   
            for elem in self.search_param:
                #print(elem)
                assert(len(elem.keys())==1)
                for temp in iter(elem.keys()):
                    param = temp
                    bounds = elem[param]
                manipulator.add_parameter(FloatParameter(param, bounds['lb'], bounds['ub']))

        # Add parameters from parametrized trace if pertinent
        cmd_param_trace = get_unique_cmd(self.cmd_run, 'get_param_trace')
        if cmd_param_trace is not None:
            arg_param_trace = cmd_param_trace['get_param_trace']
            if 'wlen' in arg_param_trace.keys():
                wlen = arg_param_trace['wlen']
            else:
                wlen = get_param_trace.default_values['win_len']

            #print("wlen = " + str(wlen))

            for t in range(0, wlen):
                name_param_trace_t = get_param_name(t)
                lb_param_trace = 1
                #ub_param_trace = default_size_all
                ub_param_trace = 8000
                manipulator.add_parameter(IntegerParameter(name_param_trace_t, lb_param_trace, ub_param_trace))

        return manipulator


    # Evaluation function for a given configuration
    def run(self, desired_result, input, limit):
        # Get the configuration values
        cfg = desired_result.configuration.data
        if self.bcreate_file:
            # If we produce the trace with get_param_trace, need a file as input produced by the tuner
            cmd_ptrace = get_unique_cmd(self.cmd_run, 'get_param_trace')
            if cmd_ptrace is not None:
                file_name_cfg = cmd_ptrace['get_param_trace']['input_cfg']
                if file_name_cfg is None:
                    file_name_cfg = get_param_trace.default_values['input_cfg']
                write_config_file(file_name_cfg, cfg)
                

            for cmd_arg in self.cmd_run:
                (cmd, _) = get_python_command(cmd_arg)
                for param in cfg:
                    cmd=re.sub(param, str(cfg[param]), cmd)
                if _debug>0: print(cmd)
                res= subprocess.getoutput(cmd)
            score=float(res)
        else:
            # Use self.memi_obj and self.trace_orig_obj  (built in function __init__)
            
            #print(self.cmd_run)
            cmd_param_trace = get_unique_cmd(self.cmd_run, 'get_param_trace')
            cmd_simu = get_unique_cmd(self.cmd_run, 'simu')
            cmd_enrich = get_unique_cmd(self.cmd_run, 'enrich')
            cmd_error = get_unique_cmd(self.cmd_run, 'error')

            # XOR between presence of simu and param_trace
            assert((cmd_param_trace is not None) != (cmd_simu is not None))

            # DEBUG
            if _debug>1:
                print(f'Start simu {cfg}')
            if cmd_simu is not None:
                (_, opts_simu) = get_python_command(cmd_simu)

                opts_simu_sub = []
                for (opt, val) in opts_simu:
                    if type(val) is not list: val_sub=[val]
                    else: val_sub=val[:]
                    for i in range(0,len(val_sub)) :
                        for param in cfg:
                            val_sub[i]=re.sub(param, str(cfg[param]), val_sub[i])
                    if len(val_sub)==1: val_sub=val_sub[0]
                    opts_simu_sub = opts_simu_sub + [(opt, val_sub)]
                values_simu = get_values_simu(opts_simu_sub)
                values_simu['memi_input'] = self.memi_obj
                if values_simu['memi_input'] is None: values_simu['inputf']=cmd_simu['simu']['input']
                values_simu['tracef'] = None

                self.trace_simu_obj = main_simulator(values_simu)
                assert(self.trace_simu_obj is not None)
            else:
                # If we don't simulate, we have a fully parametrized trace
                assert(cmd_param_trace is not None)
                (_, opts_param_trace) = get_python_command(cmd_param_trace)

                values_param_trace = get_values_paramtrace(opts_param_trace)
                values_param_trace['mparam'] = cfg
                values_param_trace['output'] = None

                self.trace_simu_obj = main_generate_parametrized_trace(values_param_trace)
                assert(self.trace_simu_obj is not None)

            #print(self.trace_simu_obj)

            # DEBUG
            if _debug>1: print("Start enrich")
            if cmd_enrich is not None:

                # work_enrich parameter : commit parameter values from cfg in this file
                for param in cfg:
                    # param / cfg[param]
                    if ('param' not in self.work_enrich or self.work_enrich['param'] is None):
                        self.work_enrich['param'] = {}
                    self.work_enrich['param'][param] = cfg[param]

                (_,opt_enrich) = get_python_command(cmd_enrich)
                values_enrich = get_values_enrich(opt_enrich)

                # If simulator ran previously
                if (self.trace_simu_obj!=None):
                    values_enrich['yaml_itrace'] = [self.trace_simu_obj] #, self.trace_orig_obj]
                
                values_enrich['otrace'] = None
                values_enrich['work_input'] = self.work_enrich

                #(self.trace_simu_obj, self.trace_orig_obj) = main_enrich_trace(values_enrich)
                self.trace_simu_obj = main_enrich_trace(values_enrich)
                self.trace_simu_obj = self.trace_simu_obj[0]
                assert(self.trace_simu_obj is not None)


            # DEBUG
            if _debug>1: print("Start error")
            (_, opts_error) = get_python_command(cmd_error)
            values_error = get_values_error_script(opts_error)
            
            if self.trace_orig_obj is None:
                error_traces_filename=cmd_error['error']['traces'].split(',')
                stream_orig=open(error_traces_filename[0], 'r')
                self.trace_orig_obj=yamlload(stream_orig, Loader=Loader)
            error = evaluate(self.trace_orig_obj['trace'], self.trace_simu_obj['trace'], False, values_error)
            score = error
            if _debug>1: print(f'error {error}')

        # Note: We could use "accuracy" field instead (but, parametrization needed of search strategies)
        # https://github.com/jansel/opentuner/blob/master/opentuner/resultsdb/models.py
        if (self.score < 0):
            self.score = score
        if (score<self.score):
            self.score = score
        return opentuner.resultsdb.models.Result(time=float(score))

    # Default objective: MinimizeTime

    # Final function (called once the autotuner is done)
    def save_final_config(self, configuration):
        global solution
        print("Autotuning done!")
        
        print("score = " + str(self.score) + " | config = " + str(configuration.data))

        solution=configuration.data.copy()
        solution['score']=self.score


def solutions_to_trace(start, solutions, filename):
    outputf = open(filename, "w")
    outputf.write('rename:\n')
    outputf.write('  all: France\n')
    outputf.write('start: \''+mem_helpers.id_to_strdate(start)+'\'\n')
    outputf.write('trace:\n')

    for t in range(0, len(solutions)):
        outputf.write('- all:\n')
        solution=solutions[t]
        for k, v in solution.items():
            outputf.write(f'    {k}: {v}\n')
    outputf.close()

def autotune_on_windows(args, configfile, full_windows, sliding_windows_width):
    global solution
    T=mem_helpers.date_to_id(last_day())+1
    if full_windows is None:
        full_windows=[-T,T-1]
    if mem_helpers.is_date(full_windows[0]):
        full_windows[0]=mem_helpers.date_to_id(full_windows[0])-T
    if mem_helpers.is_date(full_windows[1]):
        full_windows[1]=mem_helpers.date_to_id(full_windows[1])-T
    lb_full_window=full_windows[0]
    ub_full_window=full_windows[1]

    solutions = []
    for i_start in range(lb_full_window, ub_full_window - sliding_windows_width+1):
        SimuTuner.main(args, configfile, [i_start, sliding_windows_width])
        solutions.append(solution)

    return lb_full_window+T, solutions


# =======================================

default_config_file = 'param.search'

def main():
    argparser = opentuner.default_argparser()
    argparser.add_argument('--config', '-c', metavar='FILENAME', help='default is param.search')
    argparser.add_argument('--slidingwindow', metavar='INTERVAL-DATE, SLIDING-SIZE', help='defines the intervals within which window will slide')
    argparser.add_argument('--resfile', '-o',  metavar='FILENAME', help='filename for trace of results')
    argparser.add_argument('--debug', '-d', action='append', metavar='MODULENAME', help='debug mem modules')
    args=argparser.parse_args()
    
    if args.config:
        configfile = args.config
    else:
        configfile = default_config_file

    if args.debug:
        print(f'debugging {args.debug}')
        for arg in args.debug:
            if arg=='parse_seepia': parse_seepia_debug(1)
            if arg=='simu': simu_debug(1)
            if arg=='tuner': debug(2)

    if args.slidingwindow:
        if args.resfile:
            resfile=args.resfile
        else: resfile='tuner.trace'
        
        startdate, enddate, sliding=[x for x in args.slidingwindow.split(',')]

        if isinstance(sliding, str) and sliding=='-':
            sliding==7
        else: sliding=int(sliding)

        if type(startdate) is str and startdate=='-':
            start, solutions=autotune_on_windows(args, configfile, None, sliding)
        else:
            if mem_helpers.is_strint(startdate):
                startdate=int(startdate)
                assert startdate<-1, 'empty window interval'
            elif mem_helpers.is_strdate(startdate):
                startdate=mem_helpers.strdate_to_date(startdate)
                
            if type(enddate) is str and enddate=='-':
                timewindows.append(-1)
            else:
                if mem_helpers.is_strint(enddate):
                    enddate=int(enddate)
                    if isinstance(startdate, int):
                        enddate+=startdate
                        assert enddate<0, 'window interval goes beyond today. {}<=-1'.format(str(enddate))
                    else:
                        enddate=mem_helpers.id_to_date(enddate, startdate)
                elif mem_helpers.is_strdate(enddate):
                    enddate=mem_helpers.strdate_to_date(enddate)
                else: assert(False), 'unrecognized format for timewindow '+arg
            start, solutions=autotune_on_windows(args, configfile, [startdate, enddate], sliding)
        solutions_to_trace(start, solutions, resfile)
        
    else:
        SimuTuner.main(args, configfile)

# DEBUG
#config=open('param.search', 'r')
#search_param_yaml=yamlload(config, Loader=Loader)
#lcommand = get_python_command(search_param_yaml['init'][0])
#print(lcommand)

if __name__ == "__main__":
    main()
