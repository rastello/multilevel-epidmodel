import os

import requests  # Installer avec 'pip install requests'


def download_table_csv(table, reset=False):
    csv_path = table + '.csv'
    if os.path.exists(csv_path) and not reset:
        print("Le fichier CSV '{}' existe déjà. Utiliser l'option 'reset=True' pour le télécharger à nouveau".format(csv_path))
        return

    api_key_file = "api_key.txt"
    if not os.path.exists(api_key_file):
        raise Error("Le fichier '{}' avec la clé api n'existe pas".format(api_key_file))

    with open(api_key_file, 'r') as f:
        api_key = f.read().strip()

    url = "https://covid-19.sante.gouv.fr/api/drees/resources/{}/download?format=csv".format(table)
    response = requests.get(url, headers={'API_KEY': api_key})
    assert response.status_code == 200, response
    with open(csv_path, 'w') as f:
        f.write(response.text)

    print("Table '{}' téléchargée dans le fichier '{}'".format(table, csv_path))




def main():
    tables={'portrait_france': False, 'portrait_reg': False, 'portrait_dep': False, 'sivic-flux_history': True, 'sursaud-covid19-quotidien-departement': True, 'sivic_panel_dep_latest': True, 'sivic_panel_france': True, 'sivic_panel_reg_latest': True, 'donnees_tests_covid_labo_quotidien': False, 'esms_history': True}
    for table, time_sensitive in tables.items():
        download_table_csv(table, time_sensitive)


if __name__ == "__main__":
    # execute only if run as a script
    main()
