# Overall description
The current version of the simulator emulates a stochastic distributed (by agent / individual) model (actually not really -- *n* {0,1}-draws with probability *p* are deterministically represented as p.n draws of 1 & exactly (1-p).n draws of 0) using aggregates.
In other words it should be more computationally efficient than a purely distributed model.
However, in the current form, only individuals that have exactly the same behavior (contact and response to infection) are aggregated.
As an example, one can easily model different counties and different categories of people (age), one cannot model non-hierachical heterogeneity associated to the handling of contacts at schools, household, work, etc. 
Future version will enable such complex modeling.
In a very near future, instead of computing a number of infected people (per day; per aggregate), a probability distribution will be output instead.

The tool is made up of five important parts.
1. The simulator (**./simu.py** written in python3) takes a input file (in yaml format) that describes each group of the population and the way it interacts (contacts) with other groups. It outputs a trace.
2. This means that such a description yaml file must be generated. **parse_seepia.py** does so based on data extracted from data.gouv.fr (provided by Sante Publique France). The dynamic of contacts, infectiousness, and contagiousness which are parameters of the model are fixed values in the yaml file (set up as arguments of parse_seepia.py here).
3. The calibration of the simulator shall use an autotuner (**tuner.py** is a wrapper to Open-tuner) that needs a metric:
4. **error.py** shall take two traces (the observed values and the simulated values) and evaluate the quality of the simulation.
5. **plot.py** takes a trace to generate some plots.

In addition, **proximity_ploting.py** plots the proximity parameter found by the autotuner over a sliding windows of dates.

The autotuner shall try several possible parameters (*N* here for the contact --uniform in the current version-- between the individuals).
For each parameter, run the simulator (that dumps a trace), evaluate the trace...

![image for overall description](doc/description.png "Main components")
# Input format for the simulator
The input file uses yaml format (called memi). 
It is made of three "sections": groups, contacts and states.
Following is an example that describes two groups (respectively R75 et R76).
For each group: size corresponds to the number of people; alias is an alias name that can be used further; contagiousity is the level of contagiousity (serial interval) of an infected individual as a function of time (here a Weibull distribution is used).
The contacts defines a *contact matrix* between groups. Here we assumed no contacts between people of different regions (lockdown).
The states is the fraction of susceptible (at the current time) and infected individuals (during the last 9 time stamps). 
The time window of size 9 is here arbitrary and fixed in hard for now and corresponds to the size of the serial interval that is used.

```
groups:  
 R76:  
   long name: Region 76  
   size: 5774185  
   contagiousity: [0, 0.05, 0.11, 0.15, 0.17, 0.16, 0.13, 0.096, 0.061, 0.034]  
 R75:  
   long name: Region 75  
   size: 5911482  
   contagiousity: [0, 0.05, 0.11, 0.15, 0.17, 0.16, 0.13, 0.096, 0.061, 0.034]  

contacts:  
 R76 from R76: 1e-06  
 R75 from R75: 1e-06  

states:  
 R76: {susceptible: 0.9999999999391144, infected: [5.5419e-06, 6.9274e-06, 6.581e-06, 3.6369e-06, 5.0224e-06, 2.4246e-06, 4.1564e-06, 4.8492e-06, 5.1955e-06, 3.2905e-06]}  
 R75: {susceptible: 0.9999999998451881, infected: [1.3871e-05, 1.404e-05, 1.4379e-05, 1.0996e-05, 1.0488e-05, 1.0657e-05, 7.7815e-06, 1.1165e-05, 1.0319e-05, 1.0657e-05]} 
```

Observe that the yaml format allows extensions: 
1.  fields can be added without hurting the parsing;
2.  also yaml anchors (&W and *W in the following example) can be used;
3.  finally expressions (`parameters['N'] / groups['R75']['size']` in the following example) can be used.

Yaml anchors should **not** be confused with parameters: a parameter (here N in the following example) will be replaced everywhere in the yaml (preprocessing phase).
For now parameters are restricted to simple scalar variables (shall be arrays in a near future).
The reason for this distinction is that parameter values as specified in the yaml (memi) input file can be overriden by the simulator itself as explained below.

Here is an example of memi file obtained from the command `python3 -m parse_seepia --memi=/dev/stdout --region=R75,R76 -N 6` that uses both anchors (defined in the vars section), parameters, and expressions:
```
metadata:
  t: -1
  date: 25 May
  phase: actes

parameters:
  N: 6.0
  Wshape: 2.24
  Wscale: 5.42

vars:
  _W: "weibull(nb_time_steps=10, shape=parameters['Wshape'], scale=parameters['Wscale'])"

groups:
  R75:
    long name: Region 75
    size: 5911482
    contagiousity: _W
  R76:
    long name: Region 76
    size: 5774185
    contagiousity: _W

contacts:
  R75 from R75: "parameters['N'] / groups['R75']['size']"
  R76 from R76: "parameters['N'] / groups['R76']['size']"

states:
  R75: {susceptible: 0.9999999998113926, infected: [8.289e-06, 5.5824e-06, 3.5524e-06, 6.9357e-06, 6.0898e-06, 4.7365e-06, 5.244e-06, 7.6123e-06, 7.274e-06, 6.0898e-06]}
  R76: {susceptible: 0.9999999999227382, infected: [2.771e-06, 2.0782e-06, 1.5587e-06, 3.6369e-06, 4.1564e-06, 2.0782e-06, 3.2905e-06, 3.2905e-06, 3.4637e-06, 2.5978e-06]}
```

# Trace format
The trace also uses yaml format.
It is made of one section names *trace* which is a list of length the number of timestamps.
For each timestamp, their is one entry per group (possibly a unique group named *all*).
For each group, size, susceptible, and infected are provided as a dictionary.
An optional section provides long names of groups usefull for pretty printing and plotting.
Following is an example obtained using command `python3 -m parse_seepia --trace=/dev/stdout --region=R75,R76 --window=-10,5` 
that dumps a trace on /dev/stdout for R75 and R76 for 5 timestamps starting at 'time' -10:
```
start: 17 May
trace:
  - R76: {size: 5774185, susceptible: 5771758, infected: 9}
    R75: {size: 5911482, susceptible: 5905191, infected: 21}
  - R76: {size: 5774185, susceptible: 5771737, infected: 21}
    R75: {size: 5911482, susceptible: 5905150, infected: 41}
  - R76: {size: 5774185, susceptible: 5771713, infected: 24}
    R75: {size: 5911482, susceptible: 5905114, infected: 36}
  - R76: {size: 5774185, susceptible: 5771701, infected: 12}
    R75: {size: 5911482, susceptible: 5905086, infected: 28}
  - R76: {size: 5774185, susceptible: 5771682, infected: 19}
    R75: {size: 5911482, susceptible: 5905055, infected: 31}

rename:
  R76: Region 76
  R75: Region 75
  ```

# Usage
Cf option --help...  
### Create input file 
As an example, one can create a memi file *example.memi*: for regions 75 & 76; 
using field *urgences*;
at time stamp -10 (i.e. 9 days before today -- today being timestamp -1);
with parameter N set to 4.
And its associated trace (on /dev/stdout) on the agregated regions (option --all) on window time [-10..-10+5[
as follow:
```python3 -m parse_seepia --memi=example.memi --region=R75,R76 --event=urgences -s -10 -N 4 --trace=/dev/stdout --all --window=-10,5```

### Run the simulator
The memi file can then be used by the simulator to itself generate a trace for 5 time stamps as follow (option *--param=N:0.567* can be used to override parameter *N* as mentioned above):
```python3 -m simu --input=example.memi --trace=/dev/stdout --all -t 5```

### Comparing traces
Hence, one can test a parameter by comparing the traces as follow:
```python3 -m parse_seepia --memi=/dev/stdout --region=R75,R76 --event=urgences -s -10 -N 0.8 --trace=original.trace --all --window=-10,5 | python3 -m simu --input=/dev/stdin --trace=simu.trace --all -t 5; python3 -m error --traces=original.trace,simu.trace --aggregate --stat=infected```

### Calibrate using auto-tuner
tuner.py uses a configuration file (in yaml format -- default required file is `param.search`) that:

1. describes the search parameters (here `__N__` , `__Wshape__` , and `__Wscale__` -- tuning contagousity Weibull parameters which is obviously meaningless with the current data is donen for the sake of clarity only). Observe that the order within which they are listed is used by the autotuner to prioritize the search.
2. optionaly specify a command to launch for the initialization (init field). Each field corresponds to an option (ex: `N : 7` corresponds to `-N 7`, `all : True` to `--all`).
3. provides the commands used for the simulation and for evaluating the error. Output of the last command is parsed as a score for autotuner.
4. asks the tuner to create the intermediate files between the different script (memi and trace files). If `create_file` is false, then a single instance of Python is run, and the logical objects corresponding to these files are directly communicated.

Here is an example (to be stored in `param.search`) where the search parameters are protected using underscores (indeed a simple search and replace is applied to the simu command by tuner.py)

```
init :
 - parse_seepia : {
  memi : temp_tuner_memi.memi,
  event : hospitalisations,
  s : -10,
  N : 7,
  trace : temp_tuner_orig.trace,
  window : '-10,9',
  all: True }

run:
  - simu : {
    input : temp_tuner_memi.memi,
    trace : temp_tuner_simu.trace,
    param : 'N:__N__',
    t : 9,
    all : True }

  - error : {
    traces : 'temp_tuner_orig.trace,temp_tuner_simu.trace',
    stat : infected,
    aggregate : True }

search_parameters:
  - __N__ : {lb: 0.5, ub: 3.0}  

create_file: false  
```

Then just launch tuner.py: `python3 -m tuner`. It returns the best configuration found and the error found between the simulated trace and the real one.

### Plots
Plots also takes a trace as an input. It can restrict to a set of groups/regions (option *--groups=...*); 
aggregate the stats of the groups (option *--aggregate*) to plot a unique line;
normalize to the total number of individuals per group (option *--normalize*). 
Here is an example (alternatively option --output can be used to store the plot in a file):
```python3 -m parse_seepia --trace=/dev/stdout | python3 -m plot --groups=R76,R75,R93,R11 --normalize --stat=infected```

![example plot](doc/plot_example.png "Output plot from previous command")

# Example
Today is May 25th. We create a trace of 7 days from March 23 as follow (will allow actual date soon):
```python3 src/parse_seepia.py -t example/before-lockdown.trace -w -65,7```

We also create a memi file for the simulator as follow:
```python3 src/parse_seepia.py -s -65 -m example/before-lockdown.memi```

We create a file `before-lockdown.search` that allows to determine parameter *N* for that period:
```
run:
  - simu : {
    input : example/before-lockdown.memi,
    trace : temp_tuner_simu.trace,
    param : 'N:__N__',
    t : 7 }
  - error: {
    traces : 'example/before-lockdown.trace,temp_tuner_simu.trace',
    stat : infected,
    aggegate : True }

search_parameters:
 - __N__: {lb: 0.5, ub: 5.0}
 
create_file: false
```

Run the auto-tuner:
```python3 src/tuner.py --config=before-lockdown.search```

It finds a value of *N* of *1.4*...

We do similar calibration for the last 7 days. First, we create  the trace:
```python3 src/parse_seepia.py -t example/before-deconfinement.trace -w -8```

Second we create the memi file:
```python3 src/parse_seepia.py -s -8 -m example/before-lockdown.memi```

Then we create a file `before-deconfinement.search`:
```
run:
  - simu : {
    input : example/before-deconfinement.memi,
    trace : temp_tuner_simu.trace,
    param : 'N:__N__',
    t : 7 }
  - error : {
    traces : 'example/before-deconfinement.trace,temp_tuner_simu.trace',
    stat : infected,
    aggregate : True }

search_parameters:
 - __N__: {lb: 0.5, ub: 5.0}

create_file: false
```

The autotuner 
```python3 src/tuner.py --config=before-deconfinement.search```

Finds a value of *N* of *1.03*.

We create traces and plot them (with original data plus optimistic and pessimistic estimations for the nest 2 weeks):
```
python3 src/parse_seepia.py -t example/full.trace  
python3 src/parse_seepia.py -s -1 -m example/now.memi   
python3 src/simu.py --input=example/now.memi --param=N:1.4 -t 15 --trace=example/pessimistic.trace  
python3 src/simu.py --input=example/now.memi --param=N:1.03 -t 15 --trace=example/optimistic.trace  
python3 src/plot.py -t example/full.trace,example/pessimistic.trace,example/optimistic.trace -s infected -a  
```

![example plot](doc/plot_example_simu.png "Output plot from previous command")

# Dependencies
The current implementation is in python3. A C version will come soon.
You need the following modules (that can be installed using *pip3 install ...*): 
*  opentuner
*  numpy
*  pyyaml
*  getopt
*  csv
*  matplotlib.pyplot
