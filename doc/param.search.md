---
init: 'python3 -m parse_seepia --memi=temp_tuner_memi.memi --event=urgences -s -10 -N 7 --trace=temp_tuner_orig.trace --window=-10,9 --all'  
run:  
  - 'python3 -m simu --input=temp_tuner_memi.memi --trace=temp_tuner_simu.trace --param=N:__N__ --param=Wshape:__Wshape__ --param=Wscale:__Wscale__  -t 9 --all'  
  - 'python3 -m error --traces=temp_tuner_orig.trace,temp_tuner_simu.trace --stat=infected --aggregate'  
search_parameters:  
  - __N__ : {lb: 0.5, ub: 5.0}  
  - __Wshape__ : {lb: 1.0, ub: 5.0}  
  - __Wscale__ : {lb: 1.0, ub: 10.0}  
---

